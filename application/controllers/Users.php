<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('user','',true);
	}

	public function index($id=null){
		$options =json_decode(file_get_contents("php://input"),true);
		echo json_encode($this->user->read($id,$options));
	}

	public function create(){
		if($this->session->userdata('logged_in')['rol']=='super'){
			$user = json_decode(file_get_contents("php://input"),true);
			$user['password'] = $this->user->get_hash($user['password']);

			$validations = $this->user->validate($user);
			if(!$validations){
				$res = $this->user->store($user);
				if($res)
					echo json_encode($res);
				else
					return $this->getOutput(array('message'=>'no se puede agregar al usuario','tag'=>'alert'));
			}else
				return $this->getOutput($validations);
		}else
			redirect('administrador','refresh');

	}

	public function update(){
		if($this->session->userdata('logged_in')['rol']=='super'){
			$user = json_decode(file_get_contents("php://input"),true);
			
			$validations = $this->user->validate($user);
			if(!$validations){
				$res = $this->user->edit($user);
				if($res)
					echo json_encode($res);
				else
					return $this->getOutput(array('message'=>'no se puede editar al usuario','tag'=>'alert'));
			}else
				return $this->getOutput($validations);
		}else
			redirect('administrador','refresh');
	}

	public function password(){
		if($this->session->userdata('logged_in')['rol']=='super'){
			$user = json_decode(file_get_contents("php://input"),true);
			$user['password'] = $this->user->get_hash($user['password']);
			
			$validations = $this->user->validate($user);
			if(!$validations){
				$res = $this->user->edit($user);
				if($res)
					echo json_encode($res);
				else
					return $this->getOutput(array('message'=>'no se puede cambiar la contraseña','tag'=>'alert'));
			}else
				return $this->getOutput($validations);
		}else
			redirect('administrador','refresh');
	}

	public function destroy($id=null){
		if($this->session->userdata('logged_in')['rol']=='super'){
			$res = $this->user->delete($id);
			if($res[0])
				echo json_encode($res[0]);
			else
				return $this->getOutput(array('message'=>'no se puede eliminar al usuario'. $res[1],'tag'=>'alert'));
		}else
			redirect('administrador','refresh');
	}

	public function login($id=null){
		$input = json_decode(file_get_contents("php://input"),true);

		$options = array(
			'where' => array(
				array(
					'field' => 'usuario',
					'value' => $input['usuario']
				)
			)
		);
		$user = $this->user->read($id,$options);
		if($user){
			if($this->user->verify($input['password'],$user[0]['password'])){

			    $sess_array = array(
			    	'rol' 		=> $user[0]['rol'],
			        'nombre' 	=> $user[0]['nombre']
			    );
			    $this->session->set_userdata('logged_in', $sess_array);
			    echo json_encode($this->session->userdata);
			    redirect('/administrador', 'refresh');
			}else
				return $this->getOutput(array('message'=>'Contraseña incorrecta','tag'=>'alert'));
		}else
			return $this->getOutput(array('message'=>'Usuario no encontrado','tag'=>'alert'));
	}

	public function getOutput($arr){
		return $this->output
				->set_content_type('application/json')
				->set_status_header('400')
				->set_output(json_encode($arr));
	}

	public function get_hash($pass){
		echo $this->user->get_hash($pass);
	}
}