<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Administrador extends CI_Controller {

	function __construct() {
		parent::__construct();
		// Load url helper
		$this->load->helper('url');
		date_default_timezone_set('America/Mexico_City');
	}

	public function index(){
		if($this->session->userdata('logged_in')){
			$data['scripts'] = array('administrador/TasksController','administrador/ModalController');
			$data['place'] = 'tareas';
			$data['ngController'] = 'TasksController';
			$data['modal'] = $this->load->view('template/vModal',null,true);
			$data['user'] = $this->session->userdata('logged_in');
			$this->load->view('admin/vAdHeader',$data);
			$this->load->view('admin/vTasks',$data);
			$this->load->view('admin/vAdFooter',$data);
		}else{
			redirect('administrador/login', 'refresh');
		}
	}

	public function login(){
		$data['scripts'] = array('administrador/LoginController');
		$data['place'] = 'login';
		$data['ngController'] = 'LoginController';
		$this->load->view('template/vHeader',$data);
		$this->load->view('admin/vLogin',$data);
		$this->load->view('template/vFooter');
	}

	public function calendario(){
		if($this->session->userdata('logged_in')){
			$data['scripts'] = array('administrador/CalendarController');
			$data['place'] = 'calendario';
			$data['ngController'] = 'CalendarController';
			$data['user'] = $this->session->userdata('logged_in');
			$this->load->view('admin/vAdHeader',$data);
			$this->load->view('admin/vCalendar',$data);
			$this->load->view('admin/vAdFooter',$data);
		}else{
			redirect('administrador','refresh');
		}
	}
	public function habitaciones(){
		if($this->session->userdata('logged_in')){
			$data['scripts'] = array('administrador/RoomsController','administrador/ModalController');
			$data['place'] = 'habitaciones';
			$data['ngController'] = 'RoomsController';
			$data['modal'] = $this->load->view('template/vModal',null,true);
			$data['user'] = $this->session->userdata('logged_in');
			$this->load->view('admin/vAdHeader',$data);
			$this->load->view('admin/vRooms',$data);
			$this->load->view('admin/vAdFooter',$data);
			
		}else{
			redirect('administrador','refresh');
		}
	}
	public function clientes(){
		if($this->session->userdata('logged_in')){
			$data['scripts'] = array('administrador/ClientsController','administrador/ModalController');
			$data['place'] = 'clientes';
			$data['ngController'] = 'ClientsController';
			$data['modal'] = $this->load->view('template/vModal',null,true);
			$data['user'] = $this->session->userdata('logged_in');
			$this->load->view('admin/vAdHeader',$data);
			$this->load->view('admin/vClients',$data);
			$this->load->view('admin/vAdFooter',$data);
		}else{
			redirect('administrador','refresh');
		}
	}
	public function reservaciones(){
		if($this->session->userdata('logged_in')){
			$data['scripts'] = array('administrador/ReservationsController','administrador/ModalController');
			$data['place'] = 'reservaciones';
			$data['ngController'] = 'ReservationsController';
			$data['modal'] = $this->load->view('template/vModal',null,true);
			$data['user'] = $this->session->userdata('logged_in');
			$this->load->view('admin/vAdHeader',$data);
			$this->load->view('admin/vReservations');
			$this->load->view('admin/vAdFooter',$data);
		}else{
			redirect('administrador','refresh');
		}
	}
	public function ingresos(){
		if($this->session->userdata('logged_in')){
			$data['scripts'] = array('administrador/RevenueController');
			$data['place'] = 'ingresos';
			$data['ngController'] = 'RevenueController';
			$data['user'] = $this->session->userdata('logged_in');
			$this->load->view('admin/vAdHeader',$data);
			$this->load->view('admin/vRevenue');
			$this->load->view('admin/vAdFooter');
		}else{
			redirect('administrador','refresh');
		}
	}

	public function usuarios(){
		if($this->session->userdata('logged_in') && $this->session->userdata('logged_in')['rol'] == 'super'){
			$data['scripts'] = array('administrador/UsersController','administrador/ModalController');
			$data['place'] = 'usuarios';
			$data['ngController'] = 'UsersController';
			$data['modal'] = $this->load->view('template/vModal',null,true);
			$data['user'] = $this->session->userdata('logged_in');
			$this->load->view('admin/vAdHeader',$data);
			$this->load->view('admin/vUsers');
			$this->load->view('admin/vAdFooter',$data);
		}else{
			redirect('administrador','refresh');
		}
	}

	public function logout(){
		$this->session->unset_userdata('logged_in');
	   	session_destroy();
	   	redirect('/administrador','refresh');
	}
}