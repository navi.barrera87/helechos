<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rooms extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('room','',true);
	}

	public function index($id=null){
		$options =json_decode(file_get_contents("php://input"),true);
		echo json_encode($this->room->read($id,$options));
	}

	public function create(){
		if($this->session->userdata('logged_in')['rol']!='guest'){
			$room = json_decode(file_get_contents("php://input"),true);

			$validations = $this->room->validate($room);
			if(!$validations){
				$res = $this->room->store($room);
				if($res)
					echo json_encode($res);
				else
					return $this->getOutput(array('message'=>'no se puede agregar la habitacion','tag'=>'alert'));
			}else
				return $this->getOutput($validations);
		}else
			return $this->getOutput(array('message'=>'No tiene los permisos para esta operacion','tag'=>'alert'));
	}

	public function update(){
		if($this->session->userdata('logged_in')['rol']!='guest'){
			$room = json_decode(file_get_contents("php://input"),true);
			
			$validations = $this->room->validate($room);
			if(!$validations){
				$res = $this->room->edit($room);
				if($res)
					echo json_encode($res);
				else
					return $this->getOutput(array('message'=>'no se puede editar la habitacion','tag'=>'alert'));
			}else
				return $this->getOutput($validations);
		}else
			return $this->getOutput(array('message'=>'No tiene los permisos para esta operacion','tag'=>'alert'));
	}

	public function destroy($id=null){
		if($this->session->userdata('logged_in')['rol']!='guest'){
			$res = $this->room->delete($id);
			if($res[0])
				echo json_encode($res[0]);
			else
				return $this->getOutput(array('message'=>'no se puede eliminar la habitacion'. $res[1],'tag'=>'alert'));
		}else
			return $this->getOutput(array('message'=>'No tiene los permisos para esta operacion','tag'=>'alert'));
	}

	public function getOutput($arr){
		return $this->output
				->set_content_type('application/json')
				->set_status_header('400')
				->set_output(json_encode($arr));
	}
}