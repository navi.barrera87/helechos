<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Revenues extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('room','',true);
		$this->load->model('reservation','',true);
	}

	public function index($id=null){
		$options =json_decode(file_get_contents("php://input"),true);
		echo json_encode($this->room->read($id,$options));
	}

	public function revenue_with_dates (){
		if($this->session->userdata('logged_in')['rol']!='guest'){
			$options = json_decode(file_get_contents("php://input"),true);
			
			$rooms = $this->room->read(null);
			if(!$rooms)
				return $this->getOutput(array('message'=>'no se pudo cargar las habitaciones','tag'=>'alert'));

			$reservations = $this->reservation->read(null,$options);
			if(!$reservations)
				return $this->getOutput(array('message'=>'no se pudo cargar las reservaciones o no hay entre fechas elegidas','tag'=>'alert'));

			$revenue = array();

			foreach ($rooms as $r => $r_value) {
				$revenue[$r] = array(
					'habitacion'=> $r_value['habitacion'],
					'falta' 	=> 0,
					'deposito' 	=> 0,
					'pagado'	=> 0,
					'balanza'	=> 0,
					'cancelado' => 0
				);
				foreach ($reservations as $re => $re_value) {
					if($r_value['habitaciones_id']==$re_value['habitaciones_id']){
						switch ($re_value['status']) {
							case 'falta de pago':
								$revenue[$r]['falta'] += $re_value['pagado'];
								$revenue[$r]['balanza'] -= $re_value['pagado'];
								break;
							case 'deposito pagado':
								$revenue[$r]['deposito'] += $re_value['pagado'];
								$revenue[$r]['balanza'] += $re_value['pagado'];
								break;
							case 'pagado':
								$revenue[$r]['pagado'] += $re_value['pagado'];
								$revenue[$r]['balanza'] += $re_value['pagado'];
								break;
							case 'cancelado':
								$revenue[$r]['cancelado'] += $re_value['pagado'];
								break;
						}
					}
				}
			}
			echo json_encode($revenue);
		}else
			return $this->getOutput(array('message'=>'No tiene los permisos para esta operacion','tag'=>'alert'));
	}

	public function getOutput($arr){
		return $this->output
				->set_content_type('application/json')
				->set_status_header('400')
				->set_output(json_encode($arr));
	}
}