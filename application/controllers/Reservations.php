<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reservations extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->model('reservation','',true);
		$this->load->model('client','',true);

		date_default_timezone_set('America/Mexico_City');
	}

	public function index($id=null){
		$options =json_decode(file_get_contents("php://input"),true);
		echo json_encode($this->reservation->read($id,$options));
	}

	public function create(){
		if($this->session->userdata('logged_in')['rol']!='guest'){
			$reservation = json_decode(file_get_contents("php://input"),true);

			$reservation['fecha'] = date('Y-m-d H:m:s');
			$canSendEmail = $reservation['enviar'];
			unset($reservation['enviar']);

			if(isset($reservation['client'])){
				$client = $reservation['client'];
				$clientValidations = $this->client->validate($client);
				if(!$clientValidations){
					unset($reservation['client']);
				}else
					return $this->getOutput($clientValidations);
			}

			$reservationValidations = $this->reservation->validate($reservation);
			if(!$reservationValidations){
				if(isset($client)){
					$res = $this->client->store($client);
					if($res)
						$reservation['clientes_id'] = $res['clientes_id'];
					else
						return $this->getOutput(array('message'=>'no se puede agregar al cliente','tag'=>'alert'));
				}

				if($this->reservation->isClientSelected($reservation)){
						$res = $this->reservation->store($reservation);
					if($res){
						if($canSendEmail){
							$toEmail = $this->reservation->read($res['reservaciones_id'],array('join'=>array(array('table'=>'clientes'),array('table'=>'habitaciones'))))[0];
							$toEmail['to'] = array($toEmail['correo'],'reservaciones@casaloshelechos.com');
							$toEmail['subject'] = 'Reservación en Casa Los Helechos';
							$this->sendEmail($toEmail);
						}
						
						echo json_encode($res);
					}else
						return $this->getOutput(array('message'=>'no se puede agregar la reservacion','tag'=>'alert'));
				}else
					return $this->getOutput(['clientes_id'=>'cliente es requerido']);
			}else
				return $this->getOutput($reservationValidations);
		}else
			return $this->getOutput(array('message'=>'No tiene los permisos para esta operacion','tag'=>'alert'));
	}

	public function update(){
		if($this->session->userdata('logged_in')['rol']!='guest'){
			$reservation = json_decode(file_get_contents("php://input"),true);

			$reservation['fecha'] = date('Y-m-d H:m:s');
			$canSendEmail = $reservation['enviar'];
			unset($reservation['enviar']);

			if(isset($reservation['client'])){
				$client = $reservation['client'];
				$clientValidations = $this->client->validate($client);
				if(!$clientValidations){
					unset($reservation['client']);
				}else
					return $this->getOutput($clientValidations);
			}

			$reservationValidations = $this->reservation->validate($reservation);
			if(!$reservationValidations){
				if(isset($client)){
					$res = $this->client->store($client);
					if($res)
						$reservation['clientes_id'] = $res['clientes_id'];
					else
						return $this->getOutput(array('message'=>'no se puede agregar al cliente','tag'=>'alert'));
				}

				if($this->reservation->isClientSelected($reservation)){
						$res = $this->reservation->edit($reservation);
					if($res){
						if($canSendEmail){
							$toEmail = $this->reservation->read($res['reservaciones_id'],array('join'=>array(array('table'=>'clientes'),array('table'=>'habitaciones'))))[0];
							$toEmail['to'] = array($toEmail['correo'],'reservaciones@casaloshelechos.com');
							$toEmail['subject'] = 'Reservación actualizada en Casa Los Helechos';
							$this->sendEmail($toEmail);
						}
						echo json_encode($res);
					}else
						return $this->getOutput(array('message'=>'no se puede editar la reservacion','tag'=>'alert'));
				}else
					return $this->getOutput(['clientes_id'=>'cliente es requerido']);
			}else
				return $this->getOutput($reservationValidations);
		}else
			return $this->getOutput(array('message'=>'No tiene los permisos para esta operacion','tag'=>'alert'));
	}

	public function destroy($id=null){
		if($this->session->userdata('logged_in')['rol']!='guest'){
			$res = $this->reservation->delete($id);
			if($res[0])
				echo json_encode($res[0]);
			else
				return $this->getOutput(array('message'=>'no se puede eliminar la reservacion'. $res[1],'tag'=>'alert'));
		}else
			return $this->getOutput(array('message'=>'No tiene los permisos para esta operacion','tag'=>'alert'));
	}

	public function sendEmail($data){
		$this->load->library('email');
	    for ($i=0; $i<count($data['to']); $i++) {

		    $this->email->from('reservaciones@casaloshelechos.com', 'Casa Los Helechos');
		    $this->email->to($data['to'][$i]);
		    $this->email->subject(utf8_decode($data['subject']));
		    $this->email->message($this->load->view('admin/vMailTemplate', $data, true));

		    $this->email->send();
		}
	}

	public function getOutput($arr){
		return $this->output
				->set_content_type('application/json')
				->set_status_header('400')
				->set_output(json_encode($arr));
	}

	public function preview(){
		$toEmail = $this->reservation->read(11,array('join'=>array(array('table'=>'clientes'),array('table'=>'habitaciones'))))[0];
		$this->load->view('admin/vMailTemplate',$toEmail);
	}
}