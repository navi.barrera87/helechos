<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PublicSite extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		// Load url helper
		$this->load->helper('url');
	}
	public function index(){
		header("Access-Control-Allow-Origin: localhost");
		header("Access-Control-Allow-Headers: Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With");
		header("Access-Control-Allow-Methods: GET, PUT, POST");
		$this->load->view('template/vPIndex');
		
	}
}