<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tasks extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('task','',true);

		date_default_timezone_set('America/Mexico_City');
	}

	public function index($id=null){
		$options =json_decode(file_get_contents("php://input"),true);
		echo json_encode($this->task->read($id,$options));
	}

	public function create(){
		if($this->session->userdata('logged_in')['rol']!='guest'){
			$task = json_decode(file_get_contents("php://input"),true);

			$validations = $this->task->validate($task);
			if(!$validations){
				$res = $this->task->store($task);
				if($res)
					echo json_encode($res);
				else
					return $this->getOutput(array('message'=>'no se puede agregar la tarea','tag'=>'alert'));
			}else
				return $this->getOutput($validations);
		}else
			return $this->getOutput(array('message'=>'No tiene los permisos para esta operacion','tag'=>'alert'));
	}

	public function update(){
		if($this->session->userdata('logged_in')['rol']!='guest'){
			$task = json_decode(file_get_contents("php://input"),true);

			$validations = $this->task->validate($task);
			if(!$validations){
				$res = $this->task->edit($task);
				if($res)
					echo json_encode($res);
				else
					return $this->getOutput(array('message'=>'no se puede editar la tarea','tag'=>'alert'));
			}else
				return $this->getOutput($validations);
		}else
			return $this->getOutput(array('message'=>'No tiene los permisos para esta operacion','tag'=>'alert'));
	}

	public function destroy($id=null){
		if($this->session->userdata('logged_in')['rol']!='guest'){
			$res = $this->task->delete($id);
			if($res[0])
				echo json_encode($res[0]);
			else
				return $this->getOutput(array('message'=>'no se puede eliminar la tarea'. $res[1],'tag'=>'alert'));
		}else
			return $this->getOutput(array('message'=>'No tiene los permisos para esta operacion','tag'=>'alert'));
	}

	public function getOutput($arr){
		return $this->output
				->set_content_type('application/json')
				->set_status_header('400')
				->set_output(json_encode($arr));
	}
}