<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Casa Los Helechos</title>
    <link rel="shortcut icon" href="favicon.ico"/>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>assets/font-awesome/css/fontawesome-all.min.css" rel="stylesheet" type="text/css">


    <!-- Theme CSS -->
    <link href="<?php echo base_url();?>assets/css/pStyle.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    
    <?php if (isset($scripts)): foreach ($scripts as $js):?>
        <script src="<?php echo base_url()."assets/js/{$js}.js ";?>" type="text/javascript"></script>
    <?php endforeach; endif;?>

</head>

<body>
    <div class="jumbotron mainTitle">
        <img src="<?php echo base_url()."assets/images/";?>los_helechos.png" alt="">
    </div>
    <div class="container">
        <div class="page-header headerTitle">
            <h1>Residencia con rica tradición familiar</h1>
            <h3>Ofrece amplias y cómodas habitaciones dentro de una casa en el Centro de San Miguel de Allende. <br>Aquí podrás disfrutar de la escencia y las tradiciones de este hermoso lugar.</h3>
        </div>
    </div>
    <div class="container thumbs">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="thumbnail">
                <img src="<?php echo base_url()."assets/images/";?>bed1.jpg" alt="">
                <div class="caption">
                    <h3>Habitación sencilla</h3>
                    <p><ul>
                        <li>Cama King size</li>
                        <li>Secador de pelo</li>
                        <li>Amenidades</li>
                        <li>Wi-Fi</li>
                        <li>TV por cable</li>
                        <li>Ventilador</li>
                        <li>Cafetera</li>
                        <li>Caja de seguridad</li>
                    </ul></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="thumbnail">
                <img src="<?php echo base_url()."assets/images/";?>bed2.jpg" alt="">
                <div class="caption">
                    <h3>Habitación doble</h3>
                    <p><ul>
                        <li>Dos camas matrimoniales</li>
                        <li>Secador de pelo</li>
                        <li>Amenidades</li>
                        <li>Wi-Fi</li>
                        <li>TV por cable</li>
                        <li>Ventilador</li>
                        <li>Cafetera</li>
                        <li>Caja de seguridad</li>
                        <li>Sala de estar</li>
                    </ul></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="thumbnail">
                <img src="<?php echo base_url()."assets/images/";?>bed3.jpg" alt="">
                <div class="caption">
                    <h3>Cercanía al corazón de San Miguel</h3>
                    <p><ul>
                        <li>Ubicación a lugares icónicos de la ciudad</li>
                        <li>Atención tradicional sanmiguelense</li>
                        <li>Experiencia cómoda y placentera</li>
                    </ul></p>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <div class="container">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 col-lg-offset-9 col-md-offset-9 col-sm-offset-8 links">
                <a href="https://www.facebook.com/Casa-Los-Helechos-822665644585408/"><i class="fab fa-facebook"></i></a>
                <span>Casa Los Helechos</span>
            </div>
        </div>
    </footer>
</body>
</html>
    