<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true" ng-controller="ModalController">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<b style="color:#339af0;">{{model.action}} {{model.title}}</b>
					<button data-dismiss="modal" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</h5>
			</div>
			<div class="modal-body">
				<form>
					<section ng-repeat="m in model.data" class="{{(m.type=='checkbox'||m.half)?'half':''}}" ng-if="(m.type!='hidden')">
						<i ng-if="(m.icon&&m.type!='datalist-plus')" class="fa fa-{{m.icon}}"></i>
						<b ng-if="(m.type=='select'||m.type=='date'||m.type=='time')">{{(m.name).replace('es_id','')}}</b>
						<input ng-if="(m.type!='select'&&m.type!='textarea'&&m.type!='number-select'&&m.type!='datalist-plus')" id="{{m.name}}" type="{{m.type}}" 
							name="{{m.name}}" step="{{(m.decimal)?'m.decimal':'0'}}" ng-model="m.value" placeholder="{{m.placeholder}}" ng-change="{{(m.method)?'this[m.method]()':''}}">

						<textarea ng-if="m.type=='textarea'" id="{{m.name}}" name="{{m.name}}" ng-model="m.value" placeholder="{{m.placeholder}}" rows="5"></textarea>
						
						<select ng-if="m.type=='select'" id="{{m.name}}" name="{{m.name}}" ng-model="m.value" ng-options="o.value as o.name for o in m.list" ng-change="{{(m.method)?'this[m.method]()':''}}">
							<option value="">-- Elije {{(m.name).replace('es_id','')}} --</option>
						</select>

						<h5 ng-if="m.type=='number-select'">
							<b>{{m.name}}</b>
							<span class="floatRight">
								<span class="label label-primary" ng-click="add(m.value)">
									<i class="fa fa-plus"></i>
								</span>
							</span>
						</h5>
						<figure id="{{m.name}}" class="moreElements" ng-if="(m.type=='number-select')" ng-repeat="v in m.value track by $index">
							<div>
								<input type="number" ng-model="v[0]">
								<select ng-model="v[1]" ng-options="o.name as o.value for o in m.list"><option value="">-- Elije {{m.name}} --</option></select>
								<span class="label label-danger" ng-click="remove(m.value,$index)"><i class="fa fa-minus"></i></span>
							</div>
						</figure>
						<figure id="{{m.name}}" class="datalist-plus" ng-if="(m.type=='datalist-plus')">
							<i class="fa fa-user"></i><b>{{(m.name=='clientes_id')?m.name.replace('s_id',''):m.name.replace('es_id','')}}</b> 
							<span class="floatRight">
								<span class="label label-primary" ng-click="this[m.method]()" ng-show="toggleClient">{{m.btn_label}}</span>
								<span class="label label-default" ng-click="removeClientFromModel()" ng-show="!toggleClient">cancelar</span>
								<span class="label label-{{(enviar)?'success':'danger'}}" ng-click="changeEnviar(enviar)">{{(enviar)?'E':'No e'}}nviar correo</span>
							</span>
							<ui-select ng-model="m.value" ng-if="toggleClient">
					            <ui-select-match placeholder="Elije un cliente...">{{$select.selected.name}}</ui-select-match>
					            <ui-select-choices repeat="i.value as i in m.list | filter: $select.search">
					              <div ng-bind-html="i.name | highlight: $select.search"></div>
					              <small ng-bind-html="i.correo | highlight: $select.search"></small>
					            </ui-select-choices>
					         </ui-select>
							<aside id="{{c.name}}" ng-if="model.client" ng-repeat="c in model.client">
								<i ng-if="c.icon" class="fa fa-{{c.icon}}"></i>
								<input ng-if="(c.type!='select'&&c.type!='textarea'&&c.type!='number-select'&&c.type!='datalist-plus')" id="{{c.name}}" type="{{c.type}}" 
									name="{{c.name}}" step="{{(c.decimal)?'c.decimal':'0'}}" ng-model="c.value" placeholder="{{c.placeholder}}">
							</aside>
							<button ng-if="model.data.client" class="btn btn-secondary" ng-click="removeClientFromModel()">Cancelar</button>
						</figure>
					</section>
				</form>
			</div>
			<div class="modal-footer">
				<span ng-show="loading" style="float:left;"><i class="fa fa-spinner fa-pulse"></i> Procesando...</span>
				<span ng-show="error" class="error" style="float:left;">{{error.msg}}<br>{{error.alert}}</span>
	        	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
	        	<button type="button" class="btn btn-primary" ng-click="send()">Enviar</button>
	      	</div>
		</div>
	</div>
</div>