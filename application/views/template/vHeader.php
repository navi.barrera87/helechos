<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Casa Los Helechos</title>
    <link rel="shortcut icon" href="favicon.ico"/>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/sweetalert2.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/angular-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>assets/font-awesome/css/fontawesome-all.min.css" rel="stylesheet" type="text/css">


    <!-- Theme CSS -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/js/jquery.js"></script>

    <!-- FIREBASE NOTIFICATIONS -->
    <script src="https://www.gstatic.com/firebasejs/3.6.6/firebase.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <!-- SweetAlert JavaScript -->
    <script src="<?php echo base_url();?>assets/js/sweetalert2.min.js"></script>

    <script src="<?php echo base_url();?>assets/js/angular.js"></script>
    <script src="https://cdn.firebase.com/libs/angularfire/2.3.0/angularfire.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jsApp.js"></script>

    <script src="<?php echo base_url();?>assets/js/angular-ui.js"></script>
    <script src="<?php echo base_url();?>assets/js/angular-sanitize.js"></script>
    
    <?php if (isset($scripts)): foreach ($scripts as $js):?>
        <script src="<?php echo base_url()."assets/js/{$js}.js ";?>" type="text/javascript"></script>
    <?php endforeach; endif;?>

    <script type="text/javascript">
        var p = '<?php echo $place;?>';
        var baseURL = '<?php echo base_url();?>';
    </script>

</head>

<body class="loginPage" ng-app="helechos" ng-controller="<?php echo $ngController;?>">
    