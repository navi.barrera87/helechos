<div class="container">
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 clientContainer">
		<button class="btn btn-primary" style="width:100%; margin-bottom:10px;" ng-click="openModal('agregar')">Agregar usuario</button>
		<figure class="clientList" ng-repeat="c in users">
			<aside><i class="fa fa-user-circle"></i></aside>
			<aside>
				<h5>
					<b>{{c.nombre}}</b>
					<span class="floatRight">
						<i class="fa fa-edit" ng-click="openModal('editar',c)"></i> 
						<i class="fa fa-trash" ng-click="removeUser(c)"></i>
					</span>
				</h5>
				<h5>
					<b>Usuario: </b> {{c.usuario}}
					<span class="floatRight"><span class="label label-info" style="cursor:pointer;" ng-click="openModal('password',c)">Cambiar contraseña</span></span>
				</h5>
			</aside>
		</figure>
	</div>
	<?php echo $modal;?>
</div>