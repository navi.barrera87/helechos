<div class="container">
	<article>
		<div class="slider">
			<div class="slideItem" ng-scroll="loadList()">
				<button class="btn btn-primary" style="width:100%; margin-bottom:10px;" ng-click="openModal('agregar')">Agregar tarea</button>
				<figure class="tasksList {{(t.status=='terminado')?'taskDone':''}}" ng-repeat="t in tasks" ng-click="showDetails(t,$event)">
					<h5>
						<b><i class="fa fa-calendar"></i> {{t.fecha.split(' ')[0]}}
							<i class="fa fa-clock"></i> {{t.fecha.split(' ')[1]}}</b>
						<span class="floatRight">
							<i class="far fa-{{statusIcon(t.status)}} statusIcon" style="font-size:26px; color:{{color(t.status)}};" ng-click="changeStatus(t)"></i>
						</span>
					</h5>
					<h5><i class="fa fa-tasks"></i> <b>{{t.status}}</b></h5>
					<h5>{{t.titulo}}</h5>
				</figure>
				<h4 class="listLoader" ng-if="listLoading"> <i class="fa fa-spinner fa-pulse"></i> Cargando... </h4>
			</div>
			<div class="slideItem">
				<button class="btn btn-secondary tasksBack" style="width:100%; margin-bottom:10px;" ng-click="showList()"><i class="fa fa-arrow-left"></i> Volver</button>
				<div class="taskDetail">
					<h4>
						<button class="btn btn-default" ng-click="openModal('editar',task)"><i class="fa fa-edit"></i> Editar</button> 
						<span class="floatRight">
							<button class="btn btn-danger" ng-click="removeTask(task)"><i class="fa fa-trash"></i> Borrar</button>
						</span>
					</h4>
					<section class="underLine">
						<h5>Detalles de la tarea <span class="floatRight"><b style="color:{{color(task.status)}};">{{task.status}}</b></span></h5>
						<h6><i class="fa fa-calendar"></i> <b>Fecha:</b> {{task.fecha.split(' ')[0]}}</h6>
						<h6><i class="fa fa-clock"></i> <b>Hora:</b> {{task.fecha.split(' ')[1]}}</h6>
					</section>
					<section class="underLine">
						<h5>{{task.titulo}}</h5>
						<p ng-bind-html="task.comentario | breakline"></p>
					</section>
				</div>
			</div>
		</div>
	</article>
	<?php echo $modal;?>
</div>