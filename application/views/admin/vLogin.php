<div class="container-fluid">
	<form id="loginForm" class="loginForm col-md-4 col-md-offset-4 col-lg-offset-4" name="loginForm">
		<h3>CASA LOS HELECHOS</h3>
		<aside>
			<b><i class="glyphicon glyphicon-user"></i></b> 
			<input type="text" name="user" id="user" ng-model="access.usuario" placeholder="Usuario">
		</aside>
		<aside>
			<b><i class="glyphicon glyphicon-lock"></i></b> 
			<input type="password" name="pass" id="pass" ng-model="access.password" placeholder="Contraseña">
		</aside>
		<h4 ng-if="error.toggle" style="color:#777;">{{error.msg}}</h4>
		<h4 class="listLoader" ng-if="loading"> <i class="fa fa-spinner fa-pulse"></i> Cargando... </h4>
		<button class="btn btn-secondary" ng-click="getAccess()">
			<b>Entrar</b> <i class="glyphicon glyphicon-circle-arrow-right"></i>
		</button>
	</form>
</div>