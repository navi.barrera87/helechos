<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
	<meta charset="utf-8">
</head>
<body>
<?php 
	setLocale(LC_TIME,'es_MX');
	$llegada = str_replace('-','/',substr($llegada,0,10));
	$partida = str_replace('-','/',substr($partida,0,10));
?>
<img src="<?php echo base_url();?>assets/images/los_helechos.png" alt="los_helechos.png" style="width:300px;">
<h3><b>Gracias por elegir Casa Los Helechos</b></h3>
Tu habitaci&oacute;n en San Miguel de Allende esta confirmada.<br>
- Casa Los Helechos te espera el <b><?php echo ucfirst(strftime('%A, %e de %B del %G',strtotime($llegada)));?></b><br>
- Pagar&aacute;s directamente en el alojamiento (solamente efectivo por el momento)
<br><br><br>
<h3><b>Detalles de la reservaci&oacute;n</b></h3>
<b>Cliente</b><br>
<?php echo utf8_decode($nombre).' '.utf8_decode($apellido);?><br><br>
<b>Entrada</b><br>
<?php echo ucfirst(strftime('%A, %e de %B del %G',strtotime($llegada)));?> (desde las 15:00)<br>
<b>Salida</b><br>
<?php echo ucfirst(strftime('%A, %e de %B del %G',strtotime($partida)));?> (hasta las 12:00)<br>
<b>Personas</b><br>
Adultos: <?php echo $adultos;?> <br><br>
<h3><b>Detalles de la habitaci&oacute;n</b></h3>
<b>Camas</b> 
<?php echo str_replace('/',', ',$camas);?><br>
<b>Precio</b> 
<?php echo $precio;?> MXN / noche<br>
<b>Total</b> 
<?php echo $total;?> MXN<br><br>


<b>Si hay alguna cancelaci&oacute;n o cambio en la reserva se solicita hacerlo por lo menos 7 d&iacute;as antes de la
llegada de la reserva. </b>
</body>
</html>