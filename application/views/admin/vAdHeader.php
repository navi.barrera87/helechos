<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Casa Los Helechos</title>
    <link rel="shortcut icon" href="favicon.ico"/>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/sweetalert2.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/angular-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>assets/font-awesome/css/fontawesome-all.min.css" rel="stylesheet" type="text/css">


    <!-- Theme CSS -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <!-- SweetAlert JavaScript -->
    <script src="<?php echo base_url();?>assets/js/sweetalert2.min.js"></script>

    <!-- FIREBASE NOTIFICATIONS -->
    <script src="https://www.gstatic.com/firebasejs/3.6.6/firebase.js"></script>

    <!-- ANGULAR -->
    <script src="<?php echo base_url();?>assets/js/angular.js"></script>
    <script src="https://cdn.firebase.com/libs/angularfire/2.3.0/angularfire.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jsApp.js"></script>

    <script src="<?php echo base_url();?>assets/js/angular-ui.js"></script>
    <script src="<?php echo base_url();?>assets/js/angular-sanitize.js"></script>
    
    <?php if (isset($scripts)): foreach ($scripts as $js):?>
        <script src="<?php echo base_url()."assets/js/{$js}.js ";?>" type="text/javascript"></script>
    <?php endforeach; endif;?>

    <script type="text/javascript">
        var p = '<?php echo $place;?>';
        var baseURL = '<?php echo base_url();?>';
    </script>

</head>

<body ng-app="helechos" ng-controller="<?php echo $ngController;?>">
    <header class="container-fluid">
        <nav class="navbar col-lg-7 col-md-7 col-xs-12">
            <ul class="nav navbar-nav col-lg-12 col-xs-12">
                <li class="{{(place=='tareas')       ? 'active':''}} col-lg-2 col-md-2 col-xs-2">
                    <a href="{{(place=='tareas') ? '#':'<?php echo base_url();?>administrador'}}"><i class="fa fa-tasks"></i><span class="menutitle">Tareas</span></a>
                </li>
                <li class="{{(place=='calendario')   ? 'active':''}} col-lg-2 col-md-2 col-xs-2">
                    <a href="{{(place=='calendario') ? '#':'<?php echo base_url();?>administrador/calendario'}}"><i class="fa fa-calendar"></i><span class="menutitle">Calendario</span></a>
                </li>
                <li class="{{(place=='habitaciones') ? 'active':''}} col-lg-2 col-md-2 col-xs-2">
                    <a href="{{(place=='habitaciones')? '#':'<?php echo base_url();?>administrador/habitaciones'}}"><i class="fa fa-bed"></i><span class="menutitle">Habitaciones</span></a>
                </li>
                <li class="{{(place=='clientes')     ? 'active':''}} col-lg-2 col-md-2 col-xs-2">
                    <a href="{{(place=='clientes') ? '#':'<?php echo base_url();?>administrador/clientes'}}"><i class="fa fa-users"></i><span class="menutitle">Clientes</span></a>
                </li>
                <li class="{{(place=='correos')      ? 'active':''}} col-lg-2 col-md-2 col-xs-2">
                    <a href="{{(place=='correos') ? '#':'<?php echo base_url();?>administrador/reservaciones'}}"><i class="fa fa-book-open"></i><span class="menutitle">Registros</span></a>
                </li>
                <li class="{{(place=='ingresos')     ? 'active':''}} col-lg-2 col-md-2 col-xs-2">
                    <a href="{{(place=='ingresos') ? '#':'<?php echo base_url();?>administrador/ingresos'}}"><i class="fa fa-dollar-sign"></i><span class="menutitle">Ingresos</span></a>
                </li>
                <?php if($user['rol']=='super'){?>
                <li class="{{(place=='usuarios')     ? 'active':''}} col-lg-2 col-md-2 col-xs-2">
                    <a href="{{(place=='usuarios') ? '#':'<?php echo base_url();?>administrador/usuarios'}}"><i class="fa fa-users-cog"></i><span class="menutitle">Usuarios</span></a>
                </li>
                <?php }?>
            </ul>
        </nav>
        <nav class="nav col-lg-4 col-lg-offset-1 col-md-5 nav-side" ng-if="place!='ingresos'">
            <ul class="nav navbar-nav col-lg-12 col-md-12">
                <li><a href="" ng-click="refresh()"><i class="fa fa-sync"></i><span class="menutitle">Actualizar</span></a></li>
                <li class="search" ng-if="place!='ingresos'">
                    <input type="text" ng-model="searchItem" placeholder="Buscar {{place}}" ng-enter="search()">
                    <a href="" ng-click="search()">
                        <i class="fa fa-search"></i>
                        <span class="menutitle">Buscar</span>
                    </a>
                    
                </li>
            </ul>
        </nav>
    </header>
    <div class="mainView">