<div class="calendarPanel" ng-resize="resizeOptions">
	<div class="controls">
		<h3>
			<i class="fa fa-chevron-left" ng-click="scrollToDate(controlDate.left)"></i>
			{{controlDate.center | controlerDate:'month'}}, {{controlDate.center | controlerDate:'day'}}  
			- {{controlDate.right | controlerDate:'month'}}, {{controlDate.right | controlerDate:'day'}} 
			<i class="fa fa-chevron-right" ng-click="scrollToDate(controlDate.right)"></i>
		</h3>
	</div>
	<div class="rooms">
		<figure style="text-align:center">
			<b>{{controlDate.center | controlerDate:'month'}}<br>{{controlDate.center | controlerDate:'year'}}</b>
		</figure>
		<figure ng-repeat="r in rooms" id="room{{r.habitaciones_id}}" data="{{r.habitacion}}">
			<i class="fa fa-bed"></i> <b>{{r.habitacion}}</b>
			<div class="disabled" ng-if="r.condicion!='disponible'"></div>
		</figure>
	</div>
	<div class="dates">
		<div class="slider" style="width:{{slider}}px" id="slider">
			<div class="sliderRow">
				<figure 
					id="{{key}}"
					ng-repeat="(key, value) in calendarDates" 
					class="day {{(value.day==(controlDate.today | controlerDate:'day')&&(value.month==(controlDate.today | controlerDate:'month')))?'currentDay':''}} 
						{{(value.weekday=='sáb'||value.weekday=='dom')?'weekend':''}}"
					ng-click="(value.day==(controlDate.today | controlerDate:'day')&&value.month==(controlDate.today | controlerDate:'month'))? scrollToDate(controlDate.today):null"
				>
					<b>{{value.day}}<br>{{value.weekday}}</b>
				</figure>
			</div>
			<div class="sliderRow" ng-repeat="r in rooms">
				<figure  id="{{key+r.habitaciones_id}}" ng-repeat="(key, value) in calendarDates" class="day {{(value.weekday=='sáb'||value.weekday=='dom')?'weekend':''}}" >
					&nbsp;
				</figure>
				<div class="disabled" ng-if="r.condicion!='disponible'"></div>
			</div>
		</div>
	</div>
</div>