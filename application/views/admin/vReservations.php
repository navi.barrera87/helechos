<div class="container">
	<article>
		<div class="slider">
			<div class="slideItem" ng-scroll="loadList()">
				<button class="btn btn-primary" style="width:100%; margin-bottom:10px;" ng-click="openModal('agregar')">Agregar reservacion</button>
				<figure class="reservationsList" ng-repeat="r in reservations" style="border-left: 4px solid {{color(r.status)}};" ng-click="showDetails(r,$event)">
					<h5>
						<i class="fa fa-user"></i> <b>{{r.nombre}} {{r.apellido}}</b>
					</h5>
					<h5>
						<i class="fa fa-bed"></i> <b>{{r.habitacion}}</b>
					</h5>
					<h5>
						<b>Llegada:</b> {{r.llegada}}
						<span class="floatRight">
							<i class="fa fa-sign-in-alt"></i> 
						</span>
					</h5>
					<h5>
						<b>Partida:</b> {{r.partida}}
						<span class="floatRight">
							<i class="fa fa-sign-out-alt"></i>
						</span>
					</h5>
					<h5>
						<b>Status: </b>{{r.status}}
						<span class="floatRight">
							<i class="fa fa-edit action" ng-click="openModal('editar',r)"></i>
							<i class="fa fa-trash" ng-click="removeReservation(r)"></i> 
						</span>
					</h5>
				</figure>
				<h4 class="listLoader" ng-if="listLoading"> <i class="fa fa-spinner fa-pulse"></i> Cargando... </h4>
			</div>
			<div class="slideItem">
				<button class="btn btn-secondary reservationsBack" style="width:100%; margin-bottom:10px;" ng-click="showList()"><i class="fa fa-arrow-left"></i> Volver</button>
				<button class="btn btn-cancel reservationsBack" style="width:100%; margin-bottom:10px;" ng-click="cancelReservation(reservation)" ng-if="reservation.status!='cancelado'">cancelar reservacion</button>
				<div class="reservationDetail">
					<section class="underLine">
						<h5>Detalles de reservacion <span class="floatRight"><b style="color: {{color(reservation.status)}};">{{reservation.status}}</b></span></h5>
						<h6><i class="fa fa-sign-in-alt"></i> <b>Llegada:</b> {{reservation.llegada}}</h6>
						<h6><i class="fa fa-sign-out-alt"></i> <b>Salida:</b> {{reservation.partida}}</h6>
						<h6>
							<i class="fa fa-male"></i> <b>Adultos:</b> {{reservation.adultos}}
							<i class="fa fa-child"></i> <b>Niños:</b> {{reservation.ninios}}
						</h6>
						<h6><i class="fa fa-dollar-sign"></i> <b>Pagado:</b> {{reservation.pagado}}</h6>
						<h6><i class="fa fa-dollar-sign"></i> <b>Total:</b> {{reservation.total}}</h6>
						<h6 ng-if="reservation.observaciones!=''"><b>Observaciones:</b></h6>
						<p ng-if="reservation.observaciones"=''>{{reservation.observaciones | breakline}}</p>
					</section>
					<section class="underLine">
						<h5>Agendado para</h5>
						<i class="fa fa-user"></i> {{reservation.nombre}} {{reservation.apellido}}<br>
						<i class="fa fa-mobile-alt"></i> {{reservation.telefono}}<br>
						<i class="fa fa-envelope"></i> {{reservation.correo}}
					</section>
					<section class="underLine">
						<h5>Habitacion</h5>
						<i class="fa fa-bed"></i> {{reservation.habitacion}}<span class="floatRight">
					<i class="fa fa-wheelchair" style="color:{{(reservation.handicap==1)?'#339af0':'#c0c0c0'}};"></i>
					<i class="fa fa-{{(reservation.fumar==1)?'smoking':'smoking-ban'}}"></i>
				</span><br>
						<span class="label label-primary beds" ng-repeat="rr in reservation.camas.split('/')">{{rr.replace('-',' ')}}</span>
					</section>
				</div>
			</div>
		</div>
	</article>

	<?php echo $modal;?>
</div>