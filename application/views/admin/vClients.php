<div class="container" ng-scroll="loadList()">
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 clientContainer">
		<button class="btn btn-primary" style="width:100%; margin-bottom:10px;" ng-click="openModal('agregar')">Agregar cliente</button>
		<figure class="clientList" ng-repeat="c in clients">
			<aside><i class="fa fa-user-circle"></i></aside>
			<aside>
				<h5>
					<b>{{c.nombre}} {{c.apellido}}</b>
					<span class="floatRight">
						<i class="fa fa-edit" ng-click="openModal('editar',c)"></i> 
						<i class="fa fa-trash" ng-click="removeClient(c)"></i>
					</span>
				</h5>
				<h5><i class="fa fa-envelope"></i> {{c.correo}}</h5>
				<h5 ng-show="{{(c.telefono!='')}}"><i class="fa fa-mobile-alt"></i> {{c.telefono}}</h5>
			</aside>
		</figure>
		<h4 class="listLoader" ng-if="listLoading"> <i class="fa fa-spinner fa-pulse"></i> Cargando... </h4>
	</div>
	<?php echo $modal;?>
</div>