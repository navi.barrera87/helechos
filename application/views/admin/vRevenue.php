<div class="container revenueContainer">
	<article>
		<div class="slider">
			<div class="slideItem">
				<form>
					<section class="half" style="margin-top:0;"><b>Desde:</b><input type="date" ng-model="date.start"></section>
					<section class="half"><b>Hasta:</b><input type="date" ng-model="date.end"></section>
					<button style="margin:0 auto 10px auto; display:block;width:50%;" class="btn btn-primary" ng-click="calculate()">Calcular</button>
				</form>
				<span class="error" ng-if="error">{{error}}</span>
				<h4 class="listLoader" ng-if="loading"> <i class="fa fa-spinner fa-pulse"></i> Cargando... </h4>
				<div class="revenueMath" ng-if="toggleDisplay">
					<h4 class="underLine"><i class="fa fa-calendar"></i> Del {{date.start | date:'yyyy-MM-dd'}} al {{date.end | date:'yyyy-MM-dd'}}</h4>
					<h5><b style="color:#d4bc00;">Deposito pagado</b><span class="floatRight">+ {{total.deposito}} MXN</span></h5>
					<h5 class="underLine"><b style="color:#73b819;">Pagado</b><span class="floatRight">+ {{total.pagado}} MXN</span></h5>
					<h5><b>EN TOTAL</b><span class="floatRight">= {{total.deposito + total.pagado}} MXN</span></h5>
					<br>
					<h5 class="underLine"><b style="color:#ff0033;">Falta de pago</b><span class="floatRight">- {{total.falta}} MXN</span></h5>
					<h5><b>Balanza</b><span class="floatRight">= {{total.balanza}} MXN</span></h5>
				</div>
				<button class="btn btn-secondary revenueBack" style="width:100%; margin-bottom:10px;" ng-click="showDetail()" ng-if="toggleDisplay">
					<i class="fa fa-arrow-right"></i> Detalles
				</button>
			</div>
			<div class="slideItem">
				<button class="btn btn-secondary revenueBack" style="width:100%; margin-bottom:10px;" ng-click="showForm()"><i class="fa fa-arrow-left"></i> Volver</button>
				<figure class="revenueList" ng-if="toggleDisplay">
					<section>
						<aside>&nbsp;</aside>
						<div style="background:#ffed61;">&nbsp;</div>
						<div style="background:#dff0d8;">&nbsp;</div>
						<div style="background:#f2dede;">&nbsp;</div>
						<div>&nbsp;</div>
					</section>
					<aside>Habitacion</aside>
					<div>Deposito Pagado</div>
					<div>Totalmente Pagado</div>
					<div>Falta de Pago</div>
					<div>Balanza</div>
				</figure>
				<figure class="revenueList" ng-repeat="r in revenues" ng-if="toggleDisplay">
					<section>
						<aside>&nbsp;</aside>
						<div style="background:#ffed61;">&nbsp;</div>
						<div style="background:#dff0d8;">&nbsp;</div>
						<div style="background:#f2dede;">&nbsp;</div>
						<div>&nbsp;</div>
					</section>
					<aside>
						<i class="fa fa-bed"></i><br>
						{{r.habitacion}}
					</aside>
					<div>{{r.deposito}}</div>
					<div>{{r.pagado}}</div>
					<div>{{r.falta}}</div>
					<div style="color:{{(r.balanza<0)?'#ef6864':'#777'}};">{{r.balanza}}</div>
				</figure>
				<figure class="revenueList" ng-if="toggleDisplay">
					<section>
						<aside>&nbsp;</aside>
						<div style="background:#ffed61;">&nbsp;</div>
						<div style="background:#dff0d8;">&nbsp;</div>
						<div style="background:#f2dede;">&nbsp;</div>
						<div>&nbsp;</div>
					</section>
					<aside>Total</aside>
					<div>{{total.deposito}}</div>
					<div>{{total.pagado}}</div>
					<div>{{total.falta}}</div>
					<div style="color:{{(total.balanza<0)?'#ef6864':'#777'}};">{{total.balanza}}</div>
				</figure>
			</div>
		</div>
	</article>
</div>