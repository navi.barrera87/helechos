<div class="container">
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 roomContainer">
		<button class="btn btn-primary" style="width:100%; margin-bottom:10px;" ng-click="openModal('agregar')">Agregar habitacion</button>
		<figure class="roomList" ng-repeat="r in rooms | filter:searchItem" style="border-left: 4px solid {{color[r.condicion]}};">
			<h5>
				<b>{{r.habitacion}}</b>
				<span class="floatRight">
					<i class="fa fa-edit" ng-click="openModal('editar',r)"></i> 
					<i class="fa fa-trash" ng-click="removeRoom(r)"></i>
				</span>
			</h5>
			<h5>
				<i class="fa fa-bed"></i>
				<span class="label label-primary beds" ng-repeat="rr in r.camas.split('/')">{{rr.replace('-',' ')}}</span>
				<span class="floatRight">
					<i class="fa fa-wheelchair" style="color:{{(r.handicap==1)?'#339af0':'#c0c0c0'}};"></i>
					<i class="fa fa-{{(r.fumar==1)?'smoking':'smoking-ban'}}"></i>
				</span>
			</h5>
			<h5>
				<b>Status: </b>{{r.condicion}}
				
				<span class="floatRight"><i class="fa fa-dollar-sign"></i>{{r.precio}} /<i class="fa fa-moon"></i></span>
			</h5>
		</figure>
	</div>
	<?php echo $modal;?>
</div>
