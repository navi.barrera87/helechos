<?php
Class Room extends CI_Model{

	public function __construct(){
		parent::__construct();
		$this->load->model('reservation','',true);
	}

	public $rules = array(
		array('field'=>'habitacion', 'label'=>'habitacion', 'rules'=>'required','errors'=>array('required'=>'%s requerido')),
		array('field'=>'fumar', 'label'=>'fumar', 'rules'=>'trim'),
		array('field'=>'handicap', 'label'=>'handicap', 'rules'=>'trim'),
		array('field'=>'precio', 'label'=>'precio', 'rules'=>'required','errors'=>array('required'=>'%s requerido')),
		array('field'=>'camas', 'label'=>'camas', 'rules'=>'required','errors'=>array('required'=>'%s requerido')),
		array('field'=>'condicion', 'label'=>'condicion', 'rules'=>'required','errors'=>array('required'=>'%s requerido'))
	);

	public function read($id,$options=null){
		$this->db->select('*');
	    $this->db->from('habitaciones a');

	    if($id!=null)
	    	$this->db->where('habitaciones_id',$id);

	    if(!empty($options)){
	    	if(isset($options['where'])){
		    	foreach ($options['where'] as $o => $wheres) {
		    		if(isset($wheres['like']))
		    			$this->db->like($wheres['field'],$wheres['value']);
		    		else if(isset($wheres['or_like']))
		    			$this->db->or_like($wheres['field'],$wheres['value']);
		    		else if(isset($wheres['custom']))
		    			$this->db->where($wheres['argument']);
		    		else if(isset($wheres['or_where']))
		    			$this->db->or_where($wheres['field'],$wheres['value']);
		    		else
		    			$this->db->where($wheres['field'],$wheres['value']);
		    	}
		    }
	    	if(isset($options['join'])){
	    		foreach ($options['join'] as $o => $join) {
	    			$this->db->join($join['table'].' z'.$o,'z'.$o.'.'.$join['table'].'_id=a.'.$join['table'].'_id','left');
	    		}
	    	}
	    	if(isset($options['sort']))
	    		$this->db->order_by($options['sort']['field'],$options['sort']['order']);

	    	if(isset($options['limit']))
	    		$this->db->limit($options['limit']['records'],$options['limit']['start']);
	    }

	    $query = $this->db->get();
	    return $query->result_array();
	}

	public function store($room){
		if($this->db->insert('habitaciones',$room)){
			$room['habitaciones_id'] = $this->db->insert_id();
			return $room;
		}else
			return false;
		
	}

	public function edit($room){
		$this->db->where('habitaciones_id',$room['habitaciones_id']);
		if($this->db->update('habitaciones',$room))
			return $room;
		else
			return false;
	}

	public function delete($id){
		if(!$this->reservation->relation($id,'habitaciones_id')){
			$this->db->where('habitaciones_id',$id);
    		return ($this->db->delete('habitaciones')) ? array(true,''):array(false,'');
		}else 
			return array(false,', esta ligada a reservaciones');

	}

	public function validate($room) {
	   	$this->load->library('form_validation');
	   	$this->form_validation->set_data($room);
	   	$this->form_validation->set_rules($this->rules);

	   	$errors = array();
	   	if ($this->form_validation->run() == FALSE){
	   		foreach ($this->rules as $r){
	   			if($this->form_validation->error($r['field'],' ',' ')!='')
	   				$errors[$r['field']] = $this->form_validation->error($r['field'],' ',' ');
	   		}
	   	}

	   	return (!empty($errors)) ? $errors: false;
	}
}	