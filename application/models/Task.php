<?php
Class Task extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public $rules = array(
		array('field'=>'titulo', 'label'=>'titulo', 'rules'=>'required','errors'=>array('required'=>'%s requerido')),		
		array('field'=>'fecha', 'label'=>'fecha', 'rules'=>'required','errors'=>array('required'=>'%s requerida')),
		array('field'=>'status', 'label'=>'status', 'rules'=>'required','errors'=>array('required'=>'%s requerido'))
	);

	public function read($id,$options=null){
		$this->db->select('*');
	    $this->db->from('tareas a');

	    if($id!=null)
	    	$this->db->where('tareas_id',$id);

	    if(!empty($options)){
	    	if(isset($options['where'])){
		    	foreach ($options['where'] as $o => $wheres) {
		    		if(isset($wheres['like']))
		    			$this->db->like($wheres['field'],$wheres['value']);
		    		else if(isset($wheres['or_like']))
		    			$this->db->or_like($wheres['field'],$wheres['value']);
		    		else if(isset($wheres['custom']))
		    			$this->db->where($wheres['argument']);
		    		else if(isset($wheres['or_where']))
		    			$this->db->or_where($wheres['field'],$wheres['value']);
		    		else
		    			$this->db->where($wheres['field'],$wheres['value']);
		    	}
		    }
	    	if(isset($options['join'])){
	    		foreach ($options['join'] as $o => $join) {
	    			$this->db->join($join['table'].' z'.$o,'z'.$o.'.'.$join['table'].'_id=a.'.$join['table'].'_id','left');
	    		}
	    	}
	    	if(isset($options['sort']))
	    		$this->db->order_by($options['sort']['field'],$options['sort']['order']);

	    	if(isset($options['limit']))
	    		$this->db->limit($options['limit']['records'],$options['limit']['start']);
	    }

	    $query = $this->db->get();
	    
	    return $query->result_array();
	}

	public function store($task){
		if($this->db->insert('tareas',$task)){
			$task['tareas_id'] = $this->db->insert_id();
			return $task;
		}else
			return false;
		
	}

	public function edit($task){
		$this->db->where('tareas_id',$task['tareas_id']);
		if($this->db->update('tareas',$task))
			return $task;
		else
			return false;
	}

	public function delete($id){
		$this->db->where('tareas_id',$id);
    	return ($this->db->delete('tareas')) ? array(true,''):array(false,'');
	}

	public function validate($task) {
	   	$this->load->library('form_validation');
	   	$this->form_validation->set_data($task);
	   	$this->form_validation->set_rules($this->rules);

	   	$errors = array();
	   	if ($this->form_validation->run() == FALSE){
	   		foreach ($this->rules as $r){
	   			if($this->form_validation->error($r['field'],' ',' ')!='')
	   				$errors[$r['field']] = $this->form_validation->error($r['field'],' ',' ');
	   		}
	   	}

	   	return (!empty($errors)) ? $errors: false;
	}
}	