<?php
Class User extends CI_Model{

	CONST HASH = PASSWORD_DEFAULT;
	CONST SALT = 'c4$4l0$h3l3ch0$';
	CONST COST = 14;

	public function __construct(){
		parent::__construct();
	}

	public $rules = array(
		array('field'=>'nombre', 'label'=>'nombre', 'rules'=>'required','errors'=>array('required'=>'%s requerido')),
		array('field'=>'usuario', 'label'=>'usuario', 'rules'=>'required','errors'=>array('required'=>'%s requerido')),
		array('field'=>'password', 'label'=>'password', 'rules'=>'required','errors'=>array('required'=>'%s requerido')),
		array('field'=>'rol', 'label'=>'rol', 'rules'=>'required','errors'=>array('required'=>'%s requerido'))
	);

	public function read($id,$options=null){
		$this->db->select('*');
	    $this->db->from('usuarios a');

	    if($id!=null)
	    	$this->db->where('usuarios_id',$id);

	    if(!empty($options)){
	    	if(isset($options['where'])){
	    		foreach ($options['where'] as $o => $wheres) {
		    		if(isset($wheres['like']))
		    			$this->db->like($wheres['field'],$wheres['value']);
		    		else if(isset($wheres['or_like']))
		    			$this->db->or_like($wheres['field'],$wheres['value']);
		    		else if(isset($wheres['custom']))
		    			$this->db->where($wheres['argument']);
		    		else if(isset($wheres['or_where']))
		    			$this->db->or_where($wheres['field'],$wheres['value']);
		    		else
		    			$this->db->where($wheres['field'],$wheres['value']);
		    	}
	    	}
	    	if(isset($options['join'])){
	    		foreach ($options['join'] as $o => $join) {
	    			$this->db->join($join['table'].' z'.$o,'z'.$o.'.'.$join['table'].'_id=a.'.$join['table'].'_id','left');
	    		}
	    	}
	    	if(isset($options['sort']))
	    		$this->db->order_by($options['sort']['field'],$options['sort']['order']);

	    	if(isset($options['limit']))
	    		$this->db->limit($options['limit']['records'],$options['limit']['start']);
	    }

	    $query = $this->db->get();
	    return $query->result_array();
	}

	public function store($user){
		$user['password'] = password_hash($user['password'].self::SALT,self::HASH);
		if($this->db->insert('usuarios',$user)){
			$user['usuarios_id'] = $this->db->insert_id();
			return $user;
		}else
			return false;
		
	}

	public function edit($user){
		$this->db->where('usuarios_id',$user['usuarios_id']);
		if($this->db->update('usuarios',$user))
			return $user;
		else
			return false;
	}

	public function delete($id){
		$this->db->where('usuarios_id',$id);
    	return ($this->db->delete('usuarios')) ? array(true,''):array(false,'');
	}

	public function get_hash($str){
		return password_hash($str.self::SALT, self::HASH);
	}

	public function verify($str,$pass){
		return password_verify($str.self::SALT,$pass);
	}

	public function validate($user) {
	   	$this->load->library('form_validation');
	   	$this->form_validation->set_data($user);
	   	$this->form_validation->set_rules($this->rules);

	   	$errors = array();
	   	if ($this->form_validation->run() == FALSE){
	   		foreach ($this->rules as $r){
	   			if($this->form_validation->error($r['field'],' ',' ')!='')
	   				$errors[$r['field']] = $this->form_validation->error($r['field'],' ',' ');
	   		}
	   	}

	   	return (!empty($errors)) ? $errors: false;
	}
}	