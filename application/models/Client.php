<?php
Class Client extends CI_Model{

	public function __construct(){
		parent::__construct();
		$this->load->model('reservation','',true);
	}

	public $rules = array(
		array('field'=>'nombre', 'label'=>'nombre', 'rules'=>'required','errors'=>array('required'=>'%s requerido')),
		array('field'=>'apellido', 'label'=>'apellido', 'rules'=>'required','errors'=>array('required'=>'%s requerido')),
		//array('field'=>'correo', 'label'=>'correo', 'rules'=>'required','errors'=>array('required'=>'%s requerido')),
	);

	public function read($id,$options=null){
		$this->db->select('*');
	    $this->db->from('clientes a');

	    if($id!=null)
	    	$this->db->where('clientes_id',$id);

	    if(!empty($options)){
	    	if(isset($options['where'])){
	    		foreach ($options['where'] as $o => $wheres) {
		    		if(isset($wheres['like']))
		    			$this->db->like($wheres['field'],$wheres['value']);
		    		else if(isset($wheres['or_like']))
		    			$this->db->or_like($wheres['field'],$wheres['value']);
		    		else if(isset($wheres['custom']))
		    			$this->db->where($wheres['argument']);
		    		else if(isset($wheres['or_where']))
		    			$this->db->or_where($wheres['field'],$wheres['value']);
		    		else
		    			$this->db->where($wheres['field'],$wheres['value']);
		    	}
	    	}
	    	if(isset($options['join'])){
	    		foreach ($options['join'] as $o => $join) {
	    			$this->db->join($join['table'].' z'.$o,'z'.$o.'.'.$join['table'].'_id=a.'.$join['table'].'_id','left');
	    		}
	    	}
	    	if(isset($options['sort']))
	    		$this->db->order_by($options['sort']['field'],$options['sort']['order']);

	    	if(isset($options['limit']))
	    		$this->db->limit($options['limit']['records'],$options['limit']['start']);
	    }

	    $query = $this->db->get();
	    return $query->result_array();
	}

	public function store($client){
		if($this->db->insert('clientes',$client)){
			$client['clientes_id'] = $this->db->insert_id();
			return $client;
		}else
			return false;
		
	}

	public function edit($client){
		$this->db->where('clientes_id',$client['clientes_id']);
		if($this->db->update('clientes',$client))
			return $client;
		else
			return false;
	}

	public function delete($id){
		if(!$this->reservation->relation($id,'clientes_id')){
			$this->db->where('clientes_id',$id);
    		return ($this->db->delete('clientes')) ? array(true,''):array(false,'');
		}else 
			return array(false,', esta ligada a reservaciones');
	}

	public function validate($client) {
	   	$this->load->library('form_validation');
	   	$this->form_validation->set_data($client);
	   	$this->form_validation->set_rules($this->rules);

	   	$errors = array();
	   	if ($this->form_validation->run() == FALSE){
	   		foreach ($this->rules as $r){
	   			if($this->form_validation->error($r['field'],' ',' ')!='')
	   				$errors[$r['field']] = $this->form_validation->error($r['field'],' ',' ');
	   		}
	   	}

	   	return (!empty($errors)) ? $errors: false;
	}
}	