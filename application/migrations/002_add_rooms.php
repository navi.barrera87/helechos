<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_rooms extends CI_Migration {

    public function up(){
        $this->dbforge->add_field(array(
            'habitaciones_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'habitacion' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'fumar' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'null' => TRUE
            ),
            'handicap' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'null' => TRUE
            ),
            'camas' => array(
                'type' => 'VARCHAR',
                'constraint' => '500'
            ),
            'precio' => array(
                'type' => 'FLOAT',
                'constraint' => '10,2'
            ),
            'condicion' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'default' => 'disponible'
            )
        ));
        $this->dbforge->add_key('habitaciones_id', TRUE);
        $this->dbforge->create_table('habitaciones',TRUE);
    }

    public function down(){
        $this->dbforge->drop_table('habitaciones',TRUE);
    }
}