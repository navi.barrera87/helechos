<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_tasks extends CI_Migration {

    public function up(){
        $this->dbforge->add_field(array(
            'tareas_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'titulo' => array(
                'type' => 'VARCHAR',
                'constraint' => '300'
            ),
            'fecha' => array(
                'type' => 'DATETIME'
            ),
            'comentario' => array(
                'type' => 'TEXT'
            ),
            'status' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'default' => 'en proceso'
            )
        ));
        $this->dbforge->add_key('tareas_id', TRUE);
        $this->dbforge->create_table('tareas',TRUE);
    }

    public function down(){
        $this->dbforge->drop_table('tareas',TRUE);
    }
}