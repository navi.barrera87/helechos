<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_users extends CI_Migration {

    public function up(){
        $this->dbforge->add_field(array(
            'usuarios_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'nombre' => array(
                'type' => 'VARCHAR',
                'constraint' => '300'
            ),
            'usuario' => array(
                'type' => 'VARCHAR',
                'constraint' => '300'
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => '300'
            ),
            'rol' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            )
        ));
        $this->dbforge->add_key('usuarios_id', TRUE);
        $this->dbforge->create_table('usuarios',TRUE);
    }

    public function down(){
        $this->dbforge->drop_table('usuarios',TRUE);
    }
}