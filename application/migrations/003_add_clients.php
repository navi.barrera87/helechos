<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_clients extends CI_Migration {

    public function up(){
        $this->dbforge->add_field(array(
            'clientes_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'nombre' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'apellido' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'telefono' => array(
                'type' => 'VARCHAR',
                'constraint' => '20',
                'null' => TRUE
            ),
            'correo' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            )
        ));
        $this->dbforge->add_key('clientes_id', TRUE);
        $this->dbforge->create_table('clientes',TRUE);
    }

    public function down(){
        $this->dbforge->drop_table('clientes',TRUE);
    }
}