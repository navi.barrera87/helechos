<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_database extends CI_Migration {

    public function up(){
        $this->load->dbutil();
        if (!$this->dbutil->database_exists('helechos')){
            $this->dbforge->create_database('helechos');
        }
    }

    public function down(){
        $this->dbforge->drop_database('helechos');
    }
}