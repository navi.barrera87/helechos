<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_reservations extends CI_Migration {

    public function up(){
        $this->load->helper('fk');
        $this->dbforge->add_field(array(
            'reservaciones_id' => array(
                'type' => 'INT',
                'constraint' => 6,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'llegada' => array(
                'type' => 'DATETIME'
            ),
            'partida' => array(
                'type' => 'DATETIME'
            ),
            'fecha' => array(
                'type' => 'DATETIME'
            ),
            'adultos' => array(
                'type' => 'INT',
                'constraint' => 2,
                'default' => 1
            ),
            'ninios' => array(
                'type' => 'INT',
                'constraint' => 2,
                'null' => TRUE,
                'default' => 0
            ),
            'total' => array(
                'type' => 'FLOAT',
                'constraint' => '10,2'
            ),
            'pagado' => array(
                'type' => 'FLOAT',
                'constraint' => '10,2'
            ),
            'status' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'observaciones' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'habitaciones_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE
            ),
            'clientes_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE
            )
        ));
        $this->dbforge->add_key('reservaciones_id', TRUE);
        $this->dbforge->create_table('reservaciones',TRUE);
        $this->db->query(add_foreign_key('reservaciones', 'habitaciones_id', 'habitaciones(habitaciones_id)', 'CASCADE', 'CASCADE'));
        $this->db->query(add_foreign_key('reservaciones', 'clientes_id', 'clientes(clientes_id)', 'CASCADE', 'CASCADE'));
    }

    public function down(){
        $this->load->helper('fk');
        $this->db->query(drop_foreign_key('reservaciones', 'clientes_id'));
        $this->db->query(drop_foreign_key('reservaciones', 'habitaciones_id'));
        $this->dbforge->drop_table('reservaciones',TRUE);
    }
}