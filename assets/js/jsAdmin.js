/*app.factory('appService',function($http) {
	return {
		getTotalVentanilla:function(){
			return $http.get('../administrador/getTotalVentanillaAll');
		},
		getTotalServicios:function(){
			return $http.get('../administrador/getTotalServiciosAll');
		},
		getTotalProyectos:function(){
			return $http.get('../administrador/getTotalProyectosAll');
		}
	}
});*/

app.filter('cut',function(){
	return function (value, wordwise, max, tail) {
            if (!value) return '';
            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                  //Also remove . and , so its gives a cleaner result.
                  if (value.charAt(lastspace-1) == '.' || value.charAt(lastspace-1) == ',') {
                    lastspace = lastspace - 1;
                  }
                  value = value.substr(0, lastspace);
                }
            }
            return value + (tail || ' …');
        };
});

app.filter('breakline',function(){
	return function(text){
		if(text!=null)
			return text.replace(/\n/g,'<br>');
		else return "";
	}
});

app.filter('percentage',function(){
	return function(n,t){
		return (n*100)/t;
	};
});

app.controller('admon',function($scope,$http,$location) {



	$scope.hacerPDF = function(place,id){
		html2canvas(document.getElementById(place), {
            onrendered: function (canvas) {
                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download(place+"_registroNum_"+id+".pdf");
            }
        });
		
	};

});