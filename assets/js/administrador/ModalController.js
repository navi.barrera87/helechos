app.controller('ModalController',function($scope,$http, firebaseService){

	$scope.crud = {
		agregar : 'create',
		editar  : 'update',
		eliminar: 'destroy',
		password: 'password'
	};

	$scope.places = {
		habitaciones : 'rooms',
		clientes	 : 'clients',
		reservaciones: 'reservations',
		tareas       : 'tasks',
		usuarios	 : 'users'
	};

	$scope.enviar = true;

	$('#modal').on('show.bs.modal',function(e){
		$scope.error = {};
	});

	$scope.close = function(){
		//$scope.model = {};
		$scope.error = {};
		$('#modal').modal('hide');
	};

	$scope.open = function(){
		$('#modal').modal('show');
	};

	$scope.add = function (arr) {
		arr.push([1,0]);
	}

	$scope.remove = function (arr,i) {
		arr.splice(i,1);
	}

	$scope.changeEnviar = function(enviar){
		$scope.enviar = !enviar;
	}

	$scope.showErrors = function(err){
		$scope.error = err;
		for(var e in $scope.error.ele){
			if($scope.model.client!=undefined && (e=='nombre'||e=='apellido'||e=='correo'))
				$('#'+e).parent().parent().addClass('sectionError');
			else 
				$('#'+e).parent().addClass('sectionError');
			$('#'+e).parent().append('<span class="error">'+$scope.error.ele[e]+'</span>');
		}
  			
	};

	$scope.hideErrors = function(){
		$('form').children().removeClass('sectionError');
		$('form').children().find('span.error').remove();
		$scope.error = {};
	}

	$scope.send = function(){
		$('button').prop('disabled', true);
		$scope.hideErrors();
		$scope.loading = true;
		var data = {};
		for (var key in $scope.model.data){
			if($scope.model.data[key].type == 'number-select'){
				var valArr = [];
				for(val in $scope.model.data[key].value)
					valArr.push($scope.model.data[key].value[val][0]+'-'+$scope.model.data[key].value[val][1]);
				data[$scope.model.data[key].name] = valArr.join('/');

				if(data[$scope.model.data[key].name].indexOf('null')>-1||data[$scope.model.data[key].name].indexOf('0')>-1){
					var ele = {};
					ele[$scope.model.data[key].name] = 'Llene los campos';
					$scope.showErrors({msg:'Error al enviar',ele: ele});
					$scope.loading = false;
					$('button').prop('disabled', false);
					return;
				}
			}else if($scope.model.title=='reservaciones' && $scope.model.data[key].name == 'clientes_id'){
				data[$scope.model.data[key].name]=$scope.model.data[key].value;
				data['enviar'] = $scope.enviar;
			}else if($scope.model.title=='reservaciones' && ($scope.model.data[key].name == 'llegada'||$scope.model.data[key].name == 'partida')){
				data[$scope.model.data[key].name]= $scope.model.data[key].value.toISOString().split('T')[0] +' '
					+(($scope.model.data[key].name == 'llegada')?'15:00:00':'12:00:00');
			}else if($scope.model.title=='tareas' && $scope.model.data[key].name == 'fecha'){
				data[$scope.model.data[key].name]= $scope.model.data[key].value.toISOString().split('T')[0] + ' '
					+ ($scope.model.data[2].value.toString().substr(16, 8));
				delete $scope.model.data[2];
			}else if($scope.model.title=='usuarios' && $scope.model.data[key].name == 'password'){
				if($scope.model.data[key].value == $scope.model.data[3].value){
					data[$scope.model.data[key].name] = $scope.model.data[key].value;
					delete $scope.model.data[3];
				}else{
					var ele = {};
					ele[$scope.model.data[key].name] = 'Contraseñas no coinciden';
					ele[$scope.model.data[3].name] = 'Contraseñas no coinciden';
					$scope.showErrors({msg:'Error al enviar',ele: ele});
					$scope.loading = false;
					$('button').prop('disabled', false);
					return;
				}
			}else
				data[$scope.model.data[key].name]=$scope.model.data[key].value;
		}
		if($scope.model.client!=undefined){
			var c = {};
			for (var key in $scope.model.client){
				c[$scope.model.client[key].name]=$scope.model.client[key].value;
			}
			data.client = c;
		}

		$http.post(baseURL+$scope.places[$scope.model.title]+'/'+$scope.crud[$scope.model.action],data)
		.then(
			function (response) {
				$scope.loading = false;
				$scope.close();
				$scope.list();
				$('button').prop('disabled', false);
				$scope.fireNotification($scope.model);
			},
			function (error) {
				$scope.loading = false;
				var err = {
					msg:(error.data.message!=undefined)?error.data.message:'Error al enviar',
					alert: (error.data.alert!=undefined)?error.data.alert:'',
					ele: error.data
				};
				$scope.showErrors(err);
				$('button').prop('disabled', false);
			}
		);
	};

	$scope.fireNotification = function(model){
		var data = {
			url: 'http://localhost/personales/helechos/administrador/'+model.title
		};
		switch(model.title){
			case 'habitaciones':
				data['body'] = (model.action=='agregar') ? 'Habitacion: '+model.data[0].value+'\nDado de alta.':'Habitacion: '+model.data[0].value+'\nCambio condicion a: '+model.data[5].value;
				break;
			case 'registros':
				if(model.action=='agregar')
					data['body'] = 'Reservacion nueva!\nLlegada: '+model.data[2].value.toISOString().split('T')[0]+'\nSalida: '+model.data[3].value.toISOString().split('T')[0]+'\nEstado: '+model.data[8].value;
				else{
					data['body'] = 'Actualizacion de reservacion\nLlegada: '+model.data[2].value.toISOString().split('T')[0]+'\nSalida: '+model.data[3].value.toISOString().split('T')[0]+'\nEstado: '+model.data[8].value;
				}
				break;
		}
		if(model.title=='habitaciones'||model.title=='registros')
			firebaseService.writeNotification(data);	
	};

});