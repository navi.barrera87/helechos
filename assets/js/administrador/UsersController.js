app.controller('UsersController',function($scope,$http,$location,$timeout, userService) {
	$scope.place = p;

	$scope.roleList = [
		{name: 'guest', value: 'guest'},
		{name: 'admin', value: 'admin'},
		{name: 'super', value: 'super'}
	];

	$scope.list = function(){
		$('.fa-sync').addClass('fa-spin');
		userService.getUsers()
		.then(
			function (response){
				$scope.users = response.data;
				$('.fa-sync').removeClass('fa-spin');
			},
			function (error){
				swal({
				  	title: 'Error!',
				  	text: error.data.message,
				  	type: 'error',
				  	confirmButtonText: 'Aceptar'
				});
				$('.fa-sync').removeClass('fa-spin');
			}
		);
	};

	$scope.list();

	$scope.refresh = function(){
		$scope.searchItem = '';
		$scope.list();
	};

	$scope.openModal = function(action, user=undefined){
		$scope.model = {
			title : 'usuarios',
			action: action,
			data : [
				{
					name:'nombre',
					value:((user==undefined)?'':user.nombre),
					type:'text',
					placeholder: 'nombre'
				},
				{
					name:'usuario',
					value:((user==undefined)?'':user.usuario),
					type:'text',
					placeholder: 'usuario'
				},
				{
					name:'password',
					value:((user==undefined)?'':user.password),
					type:'password',
					icon:'lock',
					placeholder: 'password'
				},
				{
					name:'confirm',
					value:((user==undefined)?'':user.password),
					type:'password',
					icon:'lock',
					placeholder: 'confirm password'
				},
				{
					name:'rol',
					value:((user==undefined)?'':user.rol),
					type:'select',
					list:$scope.roleList
				}
			]
		};
		if(action=='editar'||action=='password'){
			if (action=='editar'){
				$scope.model.data[2].type = 'hidden';
				$scope.model.data[3].type = 'hidden';
			}else if(action=='password'){
				$scope.model.data[0].type = 'hidden';
				$scope.model.data[1].type = 'hidden';
				$scope.model.data[4].type = 'hidden';
			}

			$scope.model.data.push({
				name:'usuarios_id',
				value:user.usuarios_id,
				type:'hidden'
			});
		}
		$('#modal').modal('show');
	};

	$scope.removeUser = function (user) {
		console.log(user);
		userService.removeUser(user.usuarios_id)
		.then(
			function (response){
				$scope.list();
			},
			function (error){
				swal({
				  	title: 'Error!',
				  	text: error.data.message,
				  	type: 'error',
				  	confirmButtonText: 'Aceptar'
				});
			}
		);
	};

	$scope.search = function(){
		var options = {
			'where': [
				{field:'nombre',value:$scope.searchItem, like:true},
				{field:'usuario',value:$scope.searchItem, or_like:true},
				{field:'rol',value:$scope.searchItem, or_like:true}
			]
		};

		userService.getUsers(undefined,options)
		.then(
			function (response){
				$scope.users = response.data;	
			},
			function (error){
				swal({
				  	title: 'Error!',
				  	text: error.data.message,
				  	type: 'error',
				  	confirmButtonText: 'Aceptar'
				});
			}
		);
	};
});