app.controller('RevenueController',function($scope,$http,$location,$timeout, revenueService) {
	$scope.place = p;

	$scope.toggleDisplay = false;
	$scope.loading = false;
	$scope.revenues = [];
	$scope.date = {
		start: new Date(),
		end: new Date()
	};
	$scope.total = {
		falta:0,
		deposito:0,
		pagado:0,
		balanza:0
	};

	$scope.options = {
		'sort':{field:'reservaciones_id', order:'desc'}
	};

	$scope.calculate = function(){
		$('button.btn-primary').prop('disabled', true);
		$scope.loading = true;
		var data = {
			start : $scope.date.start.toISOString().split('T')[0] + ' 15:00:00',
			end : $scope.date.end.toISOString().split('T')[0] + ' 15:00:00'
		};

		$scope.options.where = [
			{
				custom: true,
				argument: 'partida between \''+data.start+'\' and \''+data.end+'\''
			}
		];

		revenueService.getRoomsWithReservations($scope.options)
		.then(
			function (response){
				$scope.revenues = response.data;
				for(r in $scope.revenues){
					$scope.total.falta += parseFloat($scope.revenues[r].falta);
					$scope.total.deposito += parseFloat($scope.revenues[r].deposito);
					$scope.total.pagado += parseFloat($scope.revenues[r].pagado);
					$scope.total.balanza += parseFloat($scope.revenues[r].balanza);
				}
				$scope.toggleDisplay = true;
				$scope.error = undefined;
				$('button.btn-primary').prop('disabled', false);
				$scope.loading = false;
			},
			function (error){
				$scope.error = error.data.message;
				$scope.toggleDisplay = false;
				$('button.btn-primary').prop('disabled', false);
				$scope.loading = false;
			}
		);
	};

	$scope.showDetail = function(task,e){
			$('.revenueDetail').css('display','block');
			if($(window).width()<569){
				$('.slider').css('left','-100%');
			}else
				$('.tasksBack.btn-cancel').css('display','block');
			$(window).scrollTop(0);
	};

	$scope.showForm = function(){
		$('.slider').css('left','0%');
		if($(window).width()>568){
			$('.revenueDetail').css('display','none');
		}
	};
});