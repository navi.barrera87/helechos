app.controller('LoginController',function($scope,$http,$location, userService) {

	$scope.loading = false;
	$scope.error = {
		toggle: false,
		msg: ''
	};

	$scope.access = {};
	$scope.getAccess = function (){
		if($scope.access.usuario!=undefined && $scope.access.password!=undefined){
			$scope.error.toggle = false;
			$scope.error.msg = '';
			$scope.loading = true;

			userService.login($scope.access)
			.then(
				function (response){
					console.log(response);
					window.location.href = baseURL+'administrador'
				},
				function (error){
					$scope.error.toggle = true;
					$scope.error.msg = error.data.message;
					$scope.loading = false;
				}
			);
		}else{
			$scope.error.toggle = true;
			$scope.error.msg = 'Falta usuario o contraseña';
		}
	};
});