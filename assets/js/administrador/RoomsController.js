app.controller('RoomsController',function($scope,$http,$location, roomService, firebaseService) {
	$scope.place = p;

	$scope.statusList = [
		{name: 'disponible', value: 'disponible'},
		{name: 'mantenimiento', value: 'mantenimiento'},
		{name: 'inhabilitada', value: 'inhabilitada'}
	];

	$scope.tipoList = [
		{name: 'sencilla', value: 'sencilla'},
		{name: 'doble', value: 'doble'},
		{name: 'queen', value: 'queen'},
		{name: 'king', value: 'king'}
	];

	$scope.color = {
		disponible   : '#72b718',
		mantenimiento: '#f9de0e',
		inhabilitada : '#c0c0c0'
	};

	$scope.list = function (){
		$('.fa-sync').addClass('fa-spin');
		roomService.getRooms()
		.then(
			function (response){
				$scope.rooms = response.data;
				$('.fa-sync').removeClass('fa-spin');
			},
			function (error){
				swal({
				  	title: 'Error!',
				  	text: error.data.message,
				  	type: 'error',
				  	confirmButtonText: 'Aceptar'
				});
				$('.fa-sync').removeClass('fa-spin');
			}
		);
	};

	$scope.list();

	$scope.refresh = function(){
		$scope.searchItem = '';
		$scope.list();
	};

	$scope.search = function(){
		console.log('hit search');
	};

	$scope.openModal = function(action, room=undefined){
		if(room != undefined){
			var muebles = room.camas.split('/');
			var camas = [];
			for(m in muebles){
				var aux = muebles[m].split('-');
				camas.push([aux[0]*1,aux[1]]);
			}
		}

		$scope.model = {
			title : 'habitaciones',
			action: action,
			data : [
				{
					name:'habitacion',
					value:((room==undefined)?'':room.habitacion),
					type:'text',
					icon:'bed',
					placeholder: 'nombre de habitacion'
				},
				{
					name:'fumar',
					value:((room==undefined)?'':!!parseInt(room.fumar)),
					type:'checkbox',
					icon:'smoking'
				},
				{
					name:'handicap',
					value:((room==undefined)?'':!!parseInt(room.handicap)),
					type:'checkbox',
					icon:'wheelchair'
				},
				{
					name:'camas',
					value:((room==undefined)?[[1,0]]:camas),
					type:'number-select',
					list:$scope.tipoList
				},
				{
					name: 'precio',
					value:((room==undefined)?'':parseFloat(room.precio)),
					type:'number',
					icon:'dollar-sign',
					placeholder: 'precio por noche',
					decimal: '0.01'
				},
				{
					name:'condicion',
					value:((room==undefined)?'':room.condicion),
					type:'select',
					list:$scope.statusList
				}
			]
		};
		if(action=='editar')
			$scope.model.data.push({
				name:'habitaciones_id',
				value:room.habitaciones_id,
				type:'hidden'
			});
		$('#modal').modal('show');
	};

	$scope.removeRoom = function (room) {
		roomService.removeRoom(room.habitaciones_id)
		.then(
			function (response){
				$scope.list();
			},
			function (error){
				swal({
				  	title: 'Error!',
				  	text: error.data.message,
				  	type: 'error',
				  	confirmButtonText: 'Aceptar'
				});
			}
		);
	};
});