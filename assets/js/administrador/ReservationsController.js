app.controller('ReservationsController',function($scope,$http,$location,$timeout, roomService, clientService, reservationService, firebaseService) {
	$scope.place = p;

	$scope.toggleClient = true;
	$scope.listLoading = false;
	$scope.listLock = true;
	$scope.loadLock = true;
	$scope.rooms = [];
	$scope.clients = [];
	$scope.reservations = [];
	$scope.status = [
		{name:'falta de pago',value:'falta de pago'},
		{name:'deposito pagado',value:'deposito pagado'},
		{name:'pagado',value:'pagado'}
	];
	$scope.color = function(status){
		var c = '';
		switch(status){
			case 'falta de pago':
				c = '#ff0033';
				break;
			case 'deposito pagado':
				c = '#ffe200';
				break;
			case 'pagado':
				c = '#73b819';
				break;
			case 'cancelado':
				c = '#cc00ff';
				break;
			default:
				c = '#333';
				break;
		}
		return c;
	};
	$scope.pages = {start:0,records:10};
	$scope.options = {
		'join':[
				{table:'clientes'},
				{table:'habitaciones'}
		],
		'sort':{field:'llegada', order:'desc'},
		'limit':{start:$scope.pages.start,records:$scope.pages.records}
	};

	roomService.getRooms(undefined, {'where' : [{field:'condicion',value:'disponible'}]})
	.then(
		function (response){
			for(d in response.data){
				$scope.rooms.push({
					name:response.data[d].habitacion+' / '+(response.data[d].camas).split('/'),
					value:response.data[d].habitaciones_id,
					subvalue:response.data[d].precio
				});
			}
		},
		function (error){
			swal({
			 	title: 'Error!',
			  	text: error.data.message,
			  	type: 'error',
			  	confirmButtonText: 'Aceptar'
			});
		}
	);

	$scope.list = function (){
		$scope.searchItem = '';
		$scope.pages.start = 0;
		$scope.options.limit.start = 0;
		$scope.loadLock = true;
		$('.fa-sync').addClass('fa-spin');
		reservationService.getReservations(undefined,$scope.options)
		.then(
			function (response){
				$scope.reservations = response.data;
				$('.fa-sync').removeClass('fa-spin');
			},
			function (error){
				swal({
				 	title: 'Error!',
				  	text: error.data.message,
				  	type: 'error',
				  	confirmButtonText: 'Aceptar'
				});
				$('.fa-sync').removeClass('fa-spin');
			}
		);

		clientService.getClients()
		.then(
			function (response){
				$scope.clients = [];
				for(d in response.data){
					$scope.clients.push({
						name:response.data[d].nombre+' '+response.data[d].apellido,
						value:response.data[d].clientes_id,
						correo:response.data[d].correo
					});
				}
			},
			function (error){
				swal({
				 	title: 'Error!',
				  	text: error.data.message,
				  	type: 'error',
				  	confirmButtonText: 'Aceptar'
				});
			}
		);
	};

	$scope.list();

	$scope.openModal = function(action, reservation=undefined){
		$scope.toggleClient = true;
		$scope.model = {
			title : 'reservaciones',
			action: action,
			data: [
				{
					name: 'clientes_id',
					type: 'datalist-plus',
					value: ((reservation==undefined)?'':reservation.clientes_id),
					list: $scope.clients,
					icon: 'user',
					method: 'addClientToModel',
					btn_label: 'nuevo cliente'
				},
				{
					name: 'habitaciones_id',
					type: 'select',
					value: ((reservation==undefined)?'':reservation.habitaciones_id),
					list: $scope.rooms,
					icon: 'bed',
					method: 'setTotal'
				},
				{
					name: 'llegada',
					type: 'date',
					value: ((reservation==undefined)?'':new Date(reservation.llegada)),
					icon: 'sign-in-alt',
					half: true,
					method: 'setTotal'
				},
				{
					name: 'partida',
					type: 'date',
					value: ((reservation==undefined)?'':new Date(reservation.partida)),
					icon: 'sign-out-alt',
					half: true,
					method: 'setTotal'
				},
				{
					name: 'adultos',
					type: 'number',
					value: ((reservation==undefined)?'':parseInt(reservation.adultos)),
					icon: 'male',
					placeholder: 'adultos',
					decimal: '1',
					half: true
				},
				{
					name: 'ninios',
					type: 'number',
					value: ((reservation==undefined)?'':parseInt(reservation.ninios)),
					icon: 'child',
					placeholder: 'niños',
					decimal: '1',
					half: true
				},
				{
					name: 'total',
					type: 'number',
					value: ((reservation==undefined)?'':parseFloat(reservation.total)),
					icon: 'dollar-sign',
					placeholder: 'total de reservacion',
					decimal: '0.01',
					half: true
				},
				{
					name: 'pagado',
					value:((reservation==undefined)?'':parseFloat(reservation.pagado)),
					type:'number',
					icon:'dollar-sign',
					placeholder: 'cantidad pagada',
					decimal: '0.01',
					half: true,
					method: 'checkPayed'
				},
				{
					name: 'status',
					type: 'select',
					value: ((reservation==undefined)?'':reservation.status),
					list: $scope.status,

				},
				{
					name: 'observaciones',
					type: 'textarea',
					value: ((reservation==undefined)?'':reservation.observaciones),
					placeholder: 'observaciones (opcional)'
				}
			]
		};

		if(action=='editar'){
			$scope.setTotal();
			$scope.model.data.push({
				name:'reservaciones_id',
				value:reservation.reservaciones_id,
				type:'hidden'
			});
		}
		$('#modal').modal('show');
	};

	$scope.addClientToModel = function(){
		$scope.toggleClient = false;
		$scope.model.client = [
			{
				name:'nombre',
				value:'',
				type:'text',
				placeholder: 'nombre'
			},
			{
				name:'apellido',
				value:'',
				type:'text',
				placeholder: 'apellido'
			},
			{
				name:'telefono',
				value:'',
				type:'text',
				icon:'mobile-alt',
				placeholder: 'telefono'
			},
			{
				name:'correo',
				value:'',
				type:'text',
				icon: 'envelope',
				placeholder: 'correo'
			}
		];
	};

	$scope.removeClientFromModel = function (){
		delete $scope.model['client'];
		$scope.toggleClient = true;
	};

	$scope.setTotal = function(){
		$scope.room_id = $scope.model.data[1].value;
		var days = 1;
		var room = $scope.rooms.filter(function(room){
				if(parseInt(room.value)==$scope.room_id)
					return room;
		});
		if(($scope.model.data[2].value!=undefined&&$scope.model.data[2].value!='') && ($scope.model.data[3].value!=undefined&&$scope.model.data[3].value!='')){
			var di = ($scope.model.data[2].value).toISOString().split('T')[0];
			var df = ($scope.model.data[3].value).toISOString().split('T')[0];
			days = Math.floor((new Date(df)-new Date(di))/(1000*60*60*24));
		}
		$scope.model.data[6].value = parseFloat(room[0].subvalue)*days;
	};

	$scope.checkPayed = function(){
		if($scope.model.data[6].value <= $scope.model.data[7].value)
			$scope.model.data[8].value = 'pagado';
		else if($scope.model.data[7].value == 0)
			$scope.model.data[8].value = 'falta de pago';
		else if($scope.model.data[6].value > $scope.model.data[7].value)
			$scope.model.data[8].value = 'deposito pagado';

	};

	$scope.showDetails = function(reservation,e){
		if(e.target.tagName != 'I'){
			$scope.reservation = reservation;
			$('.reservationDetail').css('display','block');
			if($(window).width()<569){
				$timeout(function(){
					$('.slider').css('left','-100%');
					$('.mainView').height($('.slideItem').eq(1).height());
					$('.mainView').css('overflow','hidden');
				},100);
			}else
				$('.reservationsBack.btn-cancel').css('display','block');
			$(window).scrollTop(0);

		}
	};

	$scope.showList = function(){
		$('.slider').css('left','0%');
		$('.mainView').height($('.slideItem').eq(0).height());
		$('.mainView').css('overflow','auto');
	};

	$scope.removeReservation = function (reservation) {
		swal({
		  	title: 'Eliminar la reservacion?',
		  	text: 'Una vez confirmado no podra recuperarla...',
		  	type: 'warning',
		  	showCancelButton: true,
		  	confirmButtonColor: '#3085d6',
		  	cancelButtonColor: '#d33',
		  	confirmButtonText: 'Borrar'
		}).then(function (result){
		  	if (result) {
				reservationService.removeReservation(reservation.reservaciones_id)
				.then(
					function (response){
						swal(
				      		'Exito!',
				      		'Reservacion eliminada',
				      		'success'
				    	);
						$scope.list();
					},
					function (error){
						swal({
						  	title: 'Error!',
						  	text: error.data.message,
						  	type: 'error',
						  	confirmButtonText: 'Aceptar'
						});
					}
				);
			}
		}).done(function(){});
	};

	$scope.cancelReservation = function (reservation){
		swal({
		  	title: 'Cancelar reservacion?',
		  	text: "Puede revertir el estado editando la reservacion",
		  	type: 'warning',
		  	showCancelButton: true,
		  	confirmButtonColor: '#3085d6',
		  	cancelButtonColor: '#d33',
		  	confirmButtonText: 'Quiero cancelar'
		}).then(function (result){
		  	if (result) {
		  		reservation.status = 'cancelado';
		  		var data = {
		  			reservaciones_id: reservation.reservaciones_id,
		  			status: reservation.status,
		  			llegada: reservation.llegada,
		  			partida: reservation.partida,
		  			habitaciones_id: reservation.habitaciones_id,
		  			clientes_id: reservation.clientes_id,
		  			fecha: reservation.fecha,
		  			pagado: reservation.pagado,
		  			total: reservation.total,
		  			adultos: reservation.adultos
		  		};
		  		reservationService.updateReservation(data)
		  		.then(
		  			function (response){
		  				swal(
				      		'Exito!',
				      		'Se cancelo la reservacion',
				      		'success'
				    	);
				    	$scope.list();
				    	$scope.fireNotification(reservation);
		  			},
		  			function (error){
		  				swal({
						  	title: 'Error!',
						  	text: error.data.message,
						  	type: 'error',
						  	confirmButtonText: 'Aceptar'
						});
		  			}
		  		);
		    	
		  	}
		}).done(function(){});
	};

	$scope.refresh = function(){
		$scope.list();
	};

	$scope.search = function(){
		$scope.pages.start = 0;
		var options = {
			'where': [
				{field:'status',value:$scope.searchItem},
				{field:'llegada',value:$scope.searchItem, or_like:true},
				{field:'partida',value:$scope.searchItem, or_like:true},
				{field:'z0.nombre',value:$scope.searchItem, or_like:true},
				{field:'z0.apellido',value:$scope.searchItem, or_like:true},
				{field:'z1.habitacion',value:$scope.searchItem, or_like:true}
			],
			'join':[
				{table:'clientes'},
				{table:'habitaciones'}
			],
			'sort':{field:'llegada', order:'desc'},
			'limit':{start:$scope.pages.start,records:$scope.pages.records}
		};

		reservationService.getReservations(undefined,options)
		.then(
			function (response){
				$scope.reservations = response.data;	
			},
			function (error){
				swal({
				  	title: 'Error!',
				  	text: error.data.message,
				  	type: 'error',
				  	confirmButtonText: 'Aceptar'
				});
			}
		);
		$scope.loadLock = ($scope.searchItem!='')?false:true;

	};

	$scope.loadList = function(){
		$scope.listLoading = true;
		if($scope.listLock && $scope.loadLock){
			$scope.pages.start += $scope.pages.records;
			$scope.options.limit.start = $scope.pages.start;
			reservationService.getReservations(undefined,$scope.options)
			.then(
				function (response){
					$timeout(function() {
						if(response.data.length==0){
							$scope.pages.start -= $scope.pages.records;
							$scope.loadLock = false;
						}
						for(r in response.data)
							$scope.reservations.push(response.data[r]);

						$scope.listLoading = false;
						$scope.listLock=false;
					}, 1000);
				},
				function (error){
					swal({
					 	title: 'Error!',
					  	text: error.data.message,
					  	type: 'error',
					  	confirmButtonText: 'Aceptar'
					});
				}
			);
		}else
			$scope.listLoading = false;
	};

	$scope.fireNotification = function(reservation){
		var data = {
			body: 'Reservacion Cancelada\nNombre: '+reservation.nombre+' '+reservation.apellido+'\nFecha: '+reservation.llegada+'\nHabitacion: '+reservation.habitacion,
			url: 'http://localhost/personales/helechos/administrador/reservaciones'
		};
		$timeout(function(){
			firebaseService.writeNotification(data);	
		},1000);
	};
});