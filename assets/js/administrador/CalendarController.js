app.controller('CalendarController',function($scope,$http,$location, $timeout,$compile,roomService, reservationService/*, firebaseService*/) {

	$scope.place = p;

	$scope.controlDate = {left:new Date(),center:new Date(),right:new Date(),today:new Date()};
	$scope.rooms = {};
	$scope.reservations = {};
	$scope.calendarDates = {};
	$scope.dateLimits = {};

	$scope.areCollided;
	$scope.canScroll = true;
	$scope.displacement = 0;
	$scope.blockWidth = 70;
	$scope.slider = $scope.blockWidth;
	$scope.slideFigsBy = 0;
	$scope.color = function(status){
		var c = '';
		switch(status){
			case 'falta de pago':
				c = '#ff0033';
				break;
			case 'deposito pagado':
				c = '#ffe200';
				break;
			case 'pagado':
				c = '#73b819';
				break;
			case 'cancelado':
				c = '#cc00ff';
				break;
			default:
				c = '#333';
				break;
		}
		return c;
	};
	$scope.options = {
		'where':[
			{argument:'a.status!="cancelado"', custom:true}
		],
		'join':[
				{table:'clientes'}
		]
	};

	$scope.resizeOptions = {
		change: function(e){
			let w = e.currentTarget.innerWidth;
			if(w<=568)
				$('.dates').width('75%');
			else if(w>568 && w <= 768)
				$('.dates').width('74%');
			else if(w>768 && w <= 1199)
				$('.dates').width('80%');
			else
				$('.dates').width('90%');
			$scope.list();
		}
	};

	$scope.dragOptions = {
        start: function(e) {
        	$(e.currentTarget).addClass('dragged');
        	$scope.displacement = 0;
        },
        drag: function(x,y,container,el) {
        	var row = {}, column = {};
		    var child, figure;

		    for(let c=1; c<$(container.el).children('.sliderRow').length; c++){
		    	child = $(container.el).children('.sliderRow')[c];
		    	row.top = $(child).offset().top - container.limits.top;
		    	row.bottom = $(child).offset().top + ($(child).height()*(5/8)) - container.limits.top;
		    	if(y >= row.top && y < row.bottom){
		    		child = $(container.el).children('.sliderRow')[c];
		    		break;
		    	}else if(y > row.bottom && y <= row.bottom + $(child).height()){
		    		child = $(container.el).children('.sliderRow')[c+1];
		    		break;
		    	}
		    }
	    	$(container.el).find('.highlight').remove();

		    for(let f=0; f < $(child).children('figure').length; f++){
		    	figure = $(child).children('figure')[f];
		    	column.left = $(figure).offset().left - container.limits.left;
		    	column.right= $(figure).offset().left + $(figure).outerWidth() - container.limits.left;
		    	if(x >= column.left && x < column.right){
		    		$scope.areCollided = $scope.checkCollision(child, figure, el);
		    		if(!$scope.areCollided)
						$(figure).append('<div class="block highlight" style="width:'+$(el).outerWidth()+'px;border:1px solid '+$(el).css('background-color')+'">');
		    		break;
		    	}
		    }
		    
			    if(x>$('.dates').width()-($(el).width()*(1/2))&& $scope.canScroll){
			    	$scope.canScroll = false;
			    	let id = $(container.el).find('.highlight').parent().attr('id');
			    	if(id!=undefined){
			    		let d = $scope.controlDate.center.toLocaleDateString('es-MX').split('/');
			    		let date = new Date(d[2],(d[1]-1),d[0]);
			    		date.setDate(date.getDate()+3);
			    		let lastRight = $scope.controlDate.right;
			    		$scope.scrollToDate(date);
			    		let edgeDate = new Date(id.substr(0,4)+'-'+id.substr(4,2)+'-'+id.substr(6,2)+' 00:00:00');
			    		let gap = Math.ceil(($scope.dateLimits.end-lastRight)/(1000*60*60*24)); 
			    		if(gap > 3)
			    			$scope.displacement += 3*$scope.blockWidth;
			    		else if(gap <= 3){
			    			if(gap==0)
			    				$scope.displacement += 0;
			    			else
			    				$scope.displacement += (gap-1)*$scope.blockWidth;
			    		}
			    	}
			    }else if(x<0 && $scope.canScroll){
			    	$scope.canScroll = false;
			    	let id = $(container.el).find('.highlight').parent().attr('id');
			    	if(id!=undefined){
			    		let d = $scope.controlDate.center.toLocaleDateString('es-MX').split('/');
			    		let date = new Date(d[2],(d[1]-1),d[0]);
			    		date.setDate(date.getDate()-3);
			    		let lastLeft = $scope.controlDate.center;
			    		$scope.scrollToDate(date);
			    		let edgeDate = new Date(id.substr(0,4)+'-'+id.substr(4,2)+'-'+id.substr(6,2)+' 00:00:00');
			    		let gap = Math.ceil((lastLeft-$scope.dateLimits.start)/(1000*60*60*24));
			    		if(gap > 3)
			    			$scope.displacement -= 3*$scope.blockWidth;
			    		else if(gap <= 3){
			    			if(gap==0)
			    				$scope.displacement += 0;
			    			else
			    				$scope.displacement -= (gap)*$scope.blockWidth;
			    		}
			    	}
			    }else if(x<$('.dates').width()-($(el).width()*(1/2)) && x> ($(el).width()*(1/2))) {
			    	$scope.canScroll = true;
			    } 
				
		    return $scope.displacement;
        },
        stop: function(e, container,parent, elem, coords) {
        	var figure = ($scope.areCollided||coords==0) ? parent:$(container.el).find('.highlight').parent();
        	var block;

        	$(elem).removeClass('dragged');
        	block = $(elem).detach();
        	$(container.el).find('.highlight').remove();
        	$(block[0]).css({
        		top: '0px',
		        left: ($(figure).outerWidth()/2) + 'px'
        	});
        	if(!$scope.areCollided && coords>0)
        		$scope.rearrangeReservation(figure, block, parent);
        	$(figure).append(block[0]);
        },
        container: 'slider'
    };

	$scope.list = function(){
		$('.calendarPanel').css('opacity',0);
		$scope.rooms = {};
		$scope.reservations = {};
		$scope.calendarDates = {};
		$scope.dateLimits = {};
		$('.fa-sync').addClass('fa-spin');
		roomService.getRooms()
		.then(
			function (response) {
				for(let r of response.data)
					$scope.rooms[r.habitaciones_id] = r;
			},
			function (error){
				console.log(error);
			}
		);

		reservationService.getReservations(null, $scope.options)
		.then(
			function (response){
				$scope.reservations = response.data;
				$scope.reservations.sort((a,b) => {
					return new Date(a.llegada) - new Date(b.llegada);
				});
				$scope.dateLimits.start = new Date($scope.reservations[0].llegada);
				$scope.dateLimits.start.setDate($scope.dateLimits.start.getDate() - 10);
				$scope.reservations.sort((a,b) => {
					return new Date(b.partida) - new Date(a.partida);
				});
				$scope.dateLimits.end = new Date($scope.reservations[0].partida);
				$scope.dateLimits.end.setDate($scope.dateLimits.end.getDate() + 10);

				var i = angular.copy($scope.dateLimits.start);
				var options = {weekday:'short',year: 'numeric', month:'short', day: 'numeric'};
				var tmp = i.toLocaleDateString('es-MX',options).replace(/ de /g,' ').split(' ');
				$scope.calendarDates[i.toISOString().split('T')[0].replace(/-/g,'')] = {
					weekday: tmp[0].replace('.,',''),
					day: tmp[1],
					month: tmp[2],
					year: tmp[3]
				}; 
				$scope.slider = $scope.blockWidth;
				while(i <= $scope.dateLimits.end){
					i.setDate(i.getDate() +1);
					tmp = i.toLocaleDateString('es-MX',options).replace(/ de /g,' ').split(' ');
					$scope.calendarDates[i.toISOString().split('T')[0].replace(/-/g,'')] = {
						weekday: tmp[0].replace('.,',''),
						day: tmp[1],
						month: tmp[2],
						year: tmp[3]
					};
					$scope.slider += $scope.blockWidth;
				}
				$timeout(function(){
					$scope.slideFigsBy = parseInt($('.dates').width() / $scope.blockWidth);
					$('.dates').width($scope.slideFigsBy*$scope.blockWidth);

					$scope.placeReservations();
					$scope.scrollToDate($scope.controlDate.today);
					$('.calendarPanel').css('opacity',1);
				},100);
				
			},
			function (error){
				console.log(error);
				$('.fa-sync').removeClass('fa-spin');
			}
		);
	};

	$scope.list();

	$scope.placeReservations = function(){
		$scope.dragOptions.top = $('.sliderRow')[0].offsetHeight;
		var datesPosition = $('.slider');
		for(r of $scope.reservations){
			var el = $('#'+(r.llegada.split(' ')[0].replace(/-/g,''))+r.habitaciones_id);
			var width = Math.ceil((new Date(r.partida)-new Date(r.llegada))/(1000*60*60*24)) * $scope.blockWidth;
			var block = $('<div class="block" id="'+r.reservaciones_id+'" style="width:'+width+'px;background:'+$scope.color(r.status)+';" ng-draggable="dragOptions">');
			var center = $('<span class="center" style="width:'+(width-($scope.blockWidth/4))+'px">'+r.nombre+'<br>'+r.apellido+'</span>');
			block.append(center);
			$(el).append($compile(block)($scope));
		}
		$scope.$apply();
		$('.fa-sync').removeClass('fa-spin');
	};

	$scope.scrollToDate = function(date){
		let d = date.toLocaleDateString('es-MX').split('/');
		let dateArr = d;
		d = d[2]+''+((parseInt(d[1])<10)?'0'+d[1]:d[1])+''+((parseInt(d[0])<10)?'0'+d[0]:d[0]);	

		$scope.controlDate.center = new Date(dateArr[2],(dateArr[1]-1),dateArr[0]);
		$scope.controlDate.right = new Date(dateArr[2],(dateArr[1]-1),dateArr[0]);
		$scope.controlDate.left = new Date(dateArr[2],(dateArr[1]-1),dateArr[0]);

		$scope.controlDate.right.setDate($scope.controlDate.right.getDate() + ($scope.slideFigsBy-1));
		$scope.controlDate.left.setDate($scope.controlDate.left.getDate() - ($scope.slideFigsBy));

		if($scope.dateLimits.start>=$scope.controlDate.left)
			$scope.controlDate.left = angular.copy($scope.dateLimits.start);
		if($scope.dateLimits.end<=$scope.controlDate.right){
			let days = Math.ceil(($scope.controlDate.right-$scope.dateLimits.end)/(1000*60*60*24));
			$scope.controlDate.center.setDate($scope.controlDate.center.getDate() - days);
			$scope.controlDate.left.setDate($scope.controlDate.center.getDate() - $scope.slideFigsBy);
			d = $scope.controlDate.center.toISOString().split('T')[0].replace(/-/g,'')
			$scope.controlDate.right = angular.copy($scope.dateLimits.end);
		}
					

		let fig = $('figure.day').filter(function(i,fig){ 
			return $(fig).attr('id');
		});
		let left = fig.filter(function(i, item){
			return d===($(item).attr('id').substr(0,8));
		});
		$('.slider').css('left','-'+left[0].offsetLeft+'px');
		
	};

	$scope.refresh = function(){
		$scope.searchItem = '';
		$scope.list();
	};

	$scope.checkCollision = function (row, column, block){
		let index = $(column).index();
		var figure;
		let cnt = 0;
		// to collide with one block in front of moving
		for(let i = index; i<(index+(block.width()/$scope.blockWidth)); i++){
			figure = $(row).children('figure')[i];
			if($(figure).has('div.block').length > 0)
				cnt++;
		}
		//find a block behind you and measure if collides
		for(let i = index; i>0; i--){
			figure = $(row).children('figure')[i];
			if($(figure).has('div.block').length > 0){
				cnt = (index<i+($(figure).children('.block')[0].offsetWidth/$scope.blockWidth)) ? cnt+1:cnt+0;
				break;
			}
		}
		return cnt > 0;
	};

	$scope.rearrangeReservation = function(figure, block, parent){
		let id = figure.attr('id');
		let roomId = id.substr(8,1);
		var llegada, partida, date;
		date = new Date(id.substr(0,4)+'-'+id.substr(4,2)+'-'+id.substr(6,2)+' 00:00:00');
		llegada = date.toISOString().split('T')[0]+' 15:00:00';
		date.setDate(date.getDate()  + (block.width()/$scope.blockWidth));
		partida = date.toISOString().split('T')[0]+' 12:00:00';

		data = $scope.reservations.find(function(r){return r.reservaciones_id == block.attr('id');});
		let nombre = data.nombre;
		let apellido = data.apellido;
		delete data['apellido'];
		delete data['nombre'];
		delete data['correo'];
		delete data['telefono'];
		data.llegada = llegada;
		data.partida = partida;
		data.habitaciones_id = roomId;
		
		reservationService.updateReservation(data)
		.then(
			function (response){
				response.data['nombre'] = nombre;
				response.data['apellido'] = apellido;
				response.data['habitacion'] = $('#room'+roomId).attr('data');
				//$scope.fireNotification(response);
				$scope.refresh();
			},
			function (error){
				swal({
				  	title: 'Error!',
				  	text: error.data.habitaciones_id,
				  	type: 'error',
				  	confirmButtonText: 'Aceptar'
				});
				$(parent).append(block[0]);
			}
		);
	};

	$scope.fireNotification = function(reservation){
		var data = {
			body: 'Reservacion actualizada\nNombre: '+reservation.nombre+' '+reservation.apellido+'\nFecha: '+reservation.llegada+'\nHabitacion: '+reservation.habitacion,
			url: 'http://localhost/personales/helechos/administrador/reservaciones'
		};
		$timeout(function(){
			firebaseService.writeNotification(data);	
		},1000);
	};
});