app.controller('TasksController',function($scope,$http,$location,$timeout, taskService, firebaseService) {
	$scope.place = p;

	$scope.listLoading = false;
	$scope.listLock = true;
	$scope.loadLock = true;
	$scope.tasks = [];
	$scope.status = [
		{name:'pendiente',value:'pendiente'},
		{name:'en progreso',value:'en progreso'},
		{name:'terminado',value:'terminado'}
	];
	$scope.pages = {start:0,records:10};
	$scope.options = {
		'sort':{field:'fecha', order:'desc'},
		'limit':{start:$scope.pages.start,records:$scope.pages.records}
	};

	$scope.color = function(status){
		var c = '';
		switch(status){
			case 'terminado':
				c = '#93c59f';
				break;
			case 'en progreso':
				c = '#c0c0c0';
				break;
			case 'pendiente':
				c = '#339af0';
				break;
		}
		return c;
	};

	$scope.statusIcon = function(status){
		var i = '';
		switch(status){
			case 'terminado':
				i = 'check-square'
				break;
			case 'en progreso':
				i = 'square';
				break;
			case 'pendiente':
				i = 'play-circle';
				break;
		}
		return i;
	};

	$scope.list = function (){
		$scope.searchItem = '';
		$scope.pages.start = 0;
		$scope.options.limit.start = 0;
		$scope.loadLock = true;

		$('.fa-sync').addClass('fa-spin');
		taskService.getTasks(undefined,$scope.options)
		.then(
			function (response){
				$scope.tasks = response.data;
				$('.fa-sync').removeClass('fa-spin');
			},
			function (error){
				swal({
				 	title: 'Error!',
				  	text: error.data.message,
				  	type: 'error',
				  	confirmButtonText: 'Aceptar'
				});
				$('.fa-sync').removeClass('fa-spin');
			}
		);
	};

	$scope.list();

	$scope.openModal = function(action, task=undefined){
		if(task != undefined){
			var taskDate = task.fecha.split(' ');
		}

		$scope.model = {
			title : 'tareas',
			action: action,
			data: [
				{
					name: 'titulo',
					type: 'text',
					value: ((task==undefined)?'':task.titulo),
					icon: 'tasks',
					placeholder: 'tarea'
				},
				{
					name: 'fecha',
					type: 'date',
					value: ((task==undefined)?'':new Date(taskDate[0])),
					icon: 'calendar',
					half: true
				},
				{
					name: 'hora',
					type: 'time',
					value: ((task==undefined)?'':new Date(task.fecha)),
					icon: 'clock',
					half: true
				},
				{
					name: 'status',
					type: 'select',
					value: ((task==undefined)?'':task.status),
					list: $scope.status,
				},
				{
					name: 'comentario',
					type: 'textarea',
					value: ((task==undefined)?'':task.comentario),
					placeholder: 'comentarios (opcional)'
				}
			]
		};

		if(action=='editar'){
			$scope.model.data.push({
				name:'tareas_id',
				value:task.tareas_id,
				type:'hidden'
			});
		}
		$('#modal').modal('show');
	};

	$scope.showDetails = function(task,e){
		if(e.target.tagName != 'I'){
			$scope.task = task;
			$('.taskDetail').css('display','block');
			if($(window).width()<569){
				$timeout(function(){
					$('.slider').css('left','-100%');
					$('.mainView').height($('.slideItem').eq(1).height());
					$('.mainView').css('overflow','hidden');
				},100);
			}else
				$('.tasksBack.btn-cancel').css('display','block');
			$(window).scrollTop(0);

		}
	};

	$scope.showList = function(){
		$('.slider').css('left','0%');
		$('.mainView').height($('.slideItem').eq(0).height());
		$('.mainView').css('overflow','auto');
		if($(window).width()>568){
			$('.taskDetail').css('display','none');
		}

	};

	$scope.refresh = function(){
		$scope.list();
	};

	$scope.search = function(){
		$scope.pages.start = 0;
		var options = {
			'where': [
				{field:'status',value:$scope.searchItem, like:true},
				{field:'fecha',value:$scope.searchItem, or_like:true},
				{field:'titulo',value:$scope.searchItem, or_like:true}
			],
			'sort':{field:'fecha', order:'desc'},
			'limit':{start:$scope.pages.start,records:$scope.pages.records}
		};

		taskService.getTasks(undefined,options)
		.then(
			function (response){
				$scope.tasks = response.data;	
			},
			function (error){
				swal({
				  	title: 'Error!',
				  	text: error.data.message,
				  	type: 'error',
				  	confirmButtonText: 'Aceptar'
				});
			}
		);
		$scope.loadLock = ($scope.searchItem!='')?false:true;
	};

	$scope.loadList = function(){
		$scope.listLoading = true;
		if($scope.listLock && $scope.loadLock){
			$scope.pages.start += $scope.pages.records;
			$scope.options.limit.start = $scope.pages.start;
			taskService.getTasks(undefined,$scope.options)
			.then(
				function (response){
					$timeout(function() {
						if(response.data.length==0){
							$scope.pages.start -= $scope.pages.records;
							$scope.loadLock = false;
						}
						for(r in response.data)
							$scope.tasks.push(response.data[r]);

						$scope.listLoading = false;
						$scope.listLock=false;
					}, 1000);
				},
				function (error){
					swal({
					 	title: 'Error!',
					  	text: error.data.message,
					  	type: 'error',
					  	confirmButtonText: 'Aceptar'
					});
				}
			);
		}else
			$scope.listLoading = false;
	};

	$scope.removeTask = function (task) {
		swal({
		  	title: 'Eliminar la tarea?',
		  	text: 'Una vez confirmado no podra recuperarla...',
		  	type: 'warning',
		  	showCancelButton: true,
		  	confirmButtonColor: '#3085d6',
		  	cancelButtonColor: '#d33',
		  	confirmButtonText: 'Borrar'
		}).then(function (result){
		  	if (result) {
		  		taskService.removeTask(task.tareas_id)
				.then(
					function (response){
						swal(
					    	'Exito!',
					    	'Se elimino la tarea',
					    	'success'
					    );
						$scope.list();
						$scope.showList();
					},
					function (error){
						swal({
						  	title: 'Error!',
						  	text: error.data.message,
						  	type: 'error',
						  	confirmButtonText: 'Aceptar'
						});
					}
				);
		  	}
		}).done(function(){});
	};

	$scope.changeStatus = function (task){
		if(task.status!='terminado'){
			var title = (task.status=='pendiente')?'Comenzar tarea?':'Finalizar tarea?';
			var text = (task.status=='pendiente')?'Una vez aceptado proceda a realizar la tarea':'Asegurese de haber completado la tarea satisfactoriamente';
			var confirmButtonText = (task.status=='pendiente')?'Comenzar':'He terminado';
			task.status = (task.status=='pendiente')?'en progreso':'terminado';
			swal({
			  	title: title,
			  	text: text,
			  	type: 'warning',
			  	showCancelButton: true,
			  	confirmButtonColor: '#3085d6',
			  	cancelButtonColor: '#d33',
			  	confirmButtonText: confirmButtonText
			}).then(function (result){
			  	if (result) {
			  		taskService.updateTask(task)
			  		.then(
			  			function (response){
			  				swal(
					      		'Exito!',
					      		'Se actualizo la tarea',
					      		'success'
					    	);
					    	$scope.list();
					    	$scope.fireNotification(response.data);
			  			},
			  			function (error){
			  				swal({
							  	title: 'Error!',
							  	text: error.data.message,
							  	type: 'error',
							  	confirmButtonText: 'Aceptar'
							});
			  			}
			  		);
			    	
			  	}
			}).done(function(){});
		}
	};

	$scope.fireNotification = function(task){
		var data = {
			body: 'Tarea: '+task.titulo+'\nCambio a estado: '+task.status,
			url: 'http://localhost/personales/helechos/administrador'
		};
		$timeout(function(){
			firebaseService.writeNotification(data);	
		},2000);
	};
});