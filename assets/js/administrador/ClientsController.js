app.controller('ClientsController',function($scope,$http,$location,$timeout, clientService) {
	$scope.place = p;

	$scope.listLoading = false;
	$scope.listLock = true;
	$scope.loadLock = true;
	$scope.pages = {start:0,records:4};
	$scope.options = {
		'limit':{start:$scope.pages.start,records:$scope.pages.records}
	};

	$scope.list = function(){
		$('.fa-sync').addClass('fa-spin');
		clientService.getClients(undefined,$scope.options)
		.then(
			function (response){
				$scope.clients = response.data;
				$('.fa-sync').removeClass('fa-spin');
			},
			function (error){
				swal({
				  	title: 'Error!',
				  	text: error.data.message,
				  	type: 'error',
				  	confirmButtonText: 'Aceptar'
				});
				$('.fa-sync').removeClass('fa-spin');
			}
		);
	};

	$scope.list();

	$scope.refresh = function(){
		$scope.searchItem = '';
		$scope.pages.start = 0;
		$scope.options.limit.start = 0;
		$scope.list();
		$scope.loadLock = true;
	};

	$scope.openModal = function(action, client=undefined){
		$scope.model = {
			title : 'clientes',
			action: action,
			data : [
				{
					name:'nombre',
					value:((client==undefined)?'':client.nombre),
					type:'text',
					placeholder: 'nombre'
				},
				{
					name:'apellido',
					value:((client==undefined)?'':client.apellido),
					type:'text',
					placeholder: 'apellido'
				},
				{
					name:'telefono',
					value:((client==undefined)?'':client.telefono),
					type:'text',
					icon:'mobile-alt',
					placeholder: 'telefono'
				},
				{
					name:'correo',
					value:((client==undefined)?'':client.correo),
					type:'text',
					icon: 'envelope',
					placeholder: 'correo'
				}
			]
		};
		if(action=='editar')
			$scope.model.data.push({
				name:'clientes_id',
				value:client.clientes_id,
				type:'hidden'
			});
		$('#modal').modal('show');
	};

	$scope.removeClient = function (client) {
		clientService.removeClient(client.clientes_id)
		.then(
			function (response){
				$scope.list();
			},
			function (error){
				swal({
				  	title: 'Error!',
				  	text: error.data.message,
				  	type: 'error',
				  	confirmButtonText: 'Aceptar'
				});
			}
		);
	};

	$scope.search = function(){
		$scope.pages.start = 0;
		var options = {
			'where': [
				{field:'nombre',value:$scope.searchItem, like:true},
				{field:'apellido',value:$scope.searchItem, or_like:true},
				{field:'correo',value:$scope.searchItem, or_like:true}
			],
			'limit':{start:$scope.pages.start,records:$scope.pages.records}
		};

		clientService.getClients(undefined,options)
		.then(
			function (response){
				$scope.clients = response.data;	
			},
			function (error){
				swal({
				  	title: 'Error!',
				  	text: error.data.message,
				  	type: 'error',
				  	confirmButtonText: 'Aceptar'
				});
			}
		);
		$scope.loadLock = ($scope.searchItem!='')?false:true;

	};

	$scope.loadList = function(){
		$scope.listLoading = true;
		if($scope.listLock && $scope.loadLock){
			$scope.pages.start += $scope.pages.records;
			$scope.options.limit.start = $scope.pages.start;
			clientService.getClients(undefined,$scope.options)
			.then(
				function (response){
					console.log(response.data);
					$timeout(function() {
						if(response.data.length==0){
							$scope.pages.start -= $scope.pages.records;
							$scope.loadLock = false;
						}
						for(r in response.data)
							$scope.clients.push(response.data[r]);

						$scope.listLoading = false;
						$scope.listLock=false;
					}, 1000);
				},
				function (error){
					swal({
					 	title: 'Error!',
					  	text: error.data.message,
					  	type: 'error',
					  	confirmButtonText: 'Aceptar'
					});
				}
			);
		}else
			$scope.listLoading = false;
	};
});