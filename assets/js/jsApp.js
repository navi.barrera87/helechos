var config = {
    apiKey: "AIzaSyAFbigWRedw0LJ0lJM9xMuyP3rHVCwgkiA",
    authDomain: "casaloshelechos-d6db7.firebaseapp.com",
    databaseURL: "https://casaloshelechos-d6db7.firebaseio.com",
    projectId: "casaloshelechos-d6db7",
    storageBucket: "casaloshelechos-d6db7.appspot.com",
    messagingSenderId: "309341289863"
};
firebase.initializeApp(config);

var app = angular.module('helechos',['ngSanitize','ui.select','firebase']);

app.factory('roomService',function($http){
	return {
		getRooms: function (id, options){
			return $http.post('../rooms/'+((id!=undefined)?id:''),options);
		},
		removeRoom: function (id){
			return $http.post('../rooms/destroy/'+id);
		}
	}
});

app.factory('taskService',function($http){
	return {
		getTasks: function (id, options){
			return $http.post('tasks/'+((id!=undefined)?id:''),options);
		},
		updateTask: function(data){
			return $http.post('tasks/update/',data);
		},
		removeTask: function (id){
			return $http.post('tasks/destroy/'+id);
		}
	}
});

app.factory('clientService',function($http){
	return {
		getClients: function(id,options){
			return $http.post('../clients/'+((id!=undefined)?id:''),options);
		},
		removeClient: function (id){
			return $http.post('../clients/destroy/'+id);
		}
	}
});

app.factory('reservationService',function($http){
	return {
		getReservations: function(id,options){
			return $http.post('../reservations/'+((id!=undefined)?id:''),options);
		},
		updateReservation: function(data){
			return $http.post('../reservations/update/',data);
		},
		removeReservation: function (id){
			return $http.post('../reservations/destroy/'+id);
		}
	}
});

app.factory('revenueService',function($http){
	return {
		getRoomsWithReservations: function(options){
			return $http.post('../revenues/revenue_with_dates',options);
		}
	}
});

app.factory('userService',function($http){
	return {
		getUsers: function(id,options){
			return $http.post('../users/'+((id!=undefined)?id:''),options);
		},
		removeUser: function (id){
			return $http.post('../users/destroy/'+id);
		},
		login: function (user){
			return $http.post('../users/login/',user);
		}
	}
});

app.factory('firebaseService',function($firebaseArray,$firebaseObject,$http,userService,$http){
	var db = firebase.database();
	var tokken = '';
	var fcm_server_key = 'AAAASAYtfYc:APA91bF0pIpZQvrdEOm1WuBhPTxbDf-uh7BMMBg9KyLwZMC7PDNJgTGd9ainf3gvyloUkzKLhQt31KjyOKKGehcnJSjyuMxZSflhS7uYDMd_hY3Wc8IhHOUkleTJC2AdpPxXKfi_juH-';

    const messaging = firebase.messaging();
    messaging.requestPermission()
    .then(function (){
       	return messaging.getToken();
    })
    .then(function (token){
       	tokken = token;

       	$http({
       		method:'post',
       		url:'https://iid.googleapis.com/iid/v1/'+tokken+'/rel/topics/helechos',
       		headers: {'Content-Type': 'application/json', 'Authorization': 'key=' + fcm_server_key}
       	}).then(
       		function (topic){},
       		function (error){
       			console.log(error);
       		}
       	);

    })
    .catch(function (err){
      	console.log('ERROR:',err);
    });

    messaging.onMessage(function (payload){
       	console.log('onMessage',payload);
    });

    // Callback fired if Instance ID token is updated.
	messaging.onTokenRefresh(function() {
	  	messaging.getToken()
	  	.then(function(refreshedToken) {
	    	tokken = refreshedToken;

	    	$http({
	       		method:'post',
	       		url:'https://iid.googleapis.com/iid/v1/'+tokken+'/rel/topics/helechos',
	       		headers: {'Content-Type': 'application/json', 'Authorization': 'key=' + fcm_server_key}
	       	}).then(
	       		function (topic){},
	       		function (error){
	       			console.log(error);
	       		}
	       	);
	  	})
	  	.catch(function(err) {
	    	console.log('Unable to retrieve refreshed token ', err);
	  	});
	});
	
	return {
		testFirebase: function (){
			// create a reference to the database location where we will store our data
		    var ref = db.ref('test/');

		    // this uses AngularFire to create the synchronized array
		    return $firebaseObject(ref);
		},
		writeDatabase: function(data){
			var test = db.ref('test/'+data.id);
			test.set({
				title: data.title,
				msg: data.msg,
				time: data.time
			});
		},
		writeNotification: function (data){
			var not = {
				notification: {
					title: 'Casa Helechos',
					body: data.body,
					click_action: data.url,
					icon: baseURL+'/assets/images/helechos_icono.png'
				},
				android:{
					notification:{
						sound: 'default'
					}
				},
				apns:{
					payload:{
						aps:{
							sound:'default'
						}
					}
				},
				to: '/topics/helechos'
			};
			$http({
		      	method: "POST",
		      	dataType: 'jsonp',
		      	headers: {'Content-Type': 'application/json', 'Authorization': 'key=' + fcm_server_key},
		      	url: "https://fcm.googleapis.com/fcm/send",
		      	data: JSON.stringify(not)
		    }).then(
		    	function (response){}, 
		      	function (error){
		      		console.log(error);
		      	}
		    );
		}
	}
});

app.directive('ngEnter', function() {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function(){
                    scope.$eval(attrs.ngEnter, {'event': event});
                });
                event.preventDefault();
            }
        });
    };
});

app.directive('ngScroll',function(){
	return function (scope, element, attrs){
		element.bind('wheel', function (event){
			if(!scope.listLoading && $(window).scrollTop() == $(document).height()-$(window).height()){
				scope.listLock = true;
				scope.$apply(function(){
	                scope.$eval(attrs.ngScroll, {'event': event});
	            });
			}
		});
		element.bind('touchmove', function (event){
			if(!scope.listLoading && $(window).scrollTop() == $(document).height()-$(window).height()){
				scope.listLock = true;
				scope.$apply(function(){
	                scope.$eval(attrs.ngScroll, {'event': event});
	            });
			}
		});
	};
});

app.directive('ngResize',function($window){
	return {
		restrict: 'A',
		scope:{
			resizeOptions: '=ngResize'
		},
		link: function(scope, el, attrs){
			scope.windowWidth = $window.innerWidth;
			if(scope.resizeOptions){
				change = scope.resizeOptions.change;
			}

			angular.element($window).bind('resize',function(){
				scope.width = $window.innerWidth;
				scope.$digest();
				if(change) change(event);
			});
		}
	}
});

app.directive('ngDraggable',function($document){
	return {
		restrict: 'A',
		scope: {
			dragOptions : '=ngDraggable'
		},
		link: function(scope, el, attrs){
			var startX, startY, x = 0, y = 0;
			var displacement = 0;
			var start, stop, drag, top, parent;
			var container = {};

			var limits, parentOffset;

			// Obtain drag options
	      	if (scope.dragOptions) {
	        	start  = scope.dragOptions.start;
	        	drag   = scope.dragOptions.drag;
	        	stop   = scope.dragOptions.stop;
	        	var id = scope.dragOptions.container;
	        	top = scope.dragOptions.top;
	        	if (id) {
	            	container.el = document.getElementById(id);
	            	container.limits = container.el.getBoundingClientRect();
	        	}
	      	}

			el.bind('mousedown touchstart',function(event){
				event.preventDefault();

				container.left = parseInt(-1*$(container.el).css('left').replace('px',''));
				parent = el.parent();
				x = 0;
				y = 0;
				
				parentOffset = el.parent().offset();
				
	          	limits = {
	          		width : el[0].offsetWidth,
	          		height : el[0].offsetHeight,
	          		top : el[0].offsetTop,
	          		left: el[0].offsetLeft
	          	};

				var targetX = (limits.left+parentOffset.left)
				var targetY = (limits.top+parentOffset.top);  
			       
				var bl = $(el).detach();
				
				$(container.el).append(bl);
				if(event.type == 'mousedown'){
					startX = (event.clientX - targetX) +container.limits.left;
			    	startY = (event.clientY - targetY) +container.limits.top;
					$document.on('mousemove', eventmove);
			    	$document.on('mouseup', eventup);	
			    }else{
			    	startX = (event.originalEvent.touches[0].clientX - targetX) +container.limits.left;
			    	startY = (event.originalEvent.touches[0].clientY - targetY) +container.limits.top;
			    	$document.on('touchmove', eventmove);
			    	$document.on('touchend', eventup);	
			    }
				
			    el.css({
			       	top: targetY-container.limits.top + 'px',
			       	left: targetX-container.limits.left+container.left + 'px'
			    });

			    if (start) start(event);
			});

			function eventmove(event) {
				if(event.type == 'mousemove'){
			    	y = event.clientY - startY;
			        x = event.clientX - startX;
			    }else{
			    	y = parseInt(event.originalEvent.touches[0].clientY) - startY;
			        x = parseInt(event.originalEvent.touches[0].clientX) - startX;
			    }
		        
		        let displacement = drag(x,y,container,el);
		        setPosition(displacement);
			}

		    function eventup(event) {
		    	if(event.type == 'mouseup'){
			      	$document.unbind('mousemove', eventmove);
			      	$document.unbind('mouseup', eventup);
			    }else{
			    	$document.unbind('touchmove', eventmove);
			      	$document.unbind('touchend', eventup);
			    }
		      	if (stop) stop(event, container,parent, el,(x+y));
		    }

		    function setPosition(disp) {
		    	displacement = disp;
		        if (container) {
		          	if (x < 0) {
		            	x = 0;
		          	} else if (x > container.limits.width - limits.width) {
		            	x = container.limits.width - limits.width;
		          	}
		          	if (y < top) {
		            	y = top;
		          	} else if (y > container.limits.height - limits.height) {
		            	y = container.limits.height - limits.height;
		          	}
		        }
		        el.css({
		          	top: y + 'px',
		          	left:  x+container.left+displacement + 'px'
		        });
		    }
		}
	}
});

/*app.config(['$routeProvider','$locationProvider',function($routeProvider,$locationProvider){

	console.log(baseURL);
	$routeProvider
	.when('administrador/inicio',{
		templateURL: 'administrador/inicio',
		controller: 'InicioController'
	});
}]);

app.controller('MainCtrl',function($route, $routeParams, $location, $rootScope){
	this.$route = $route;
    this.$location = $location;
    this.$routeParams = $routeParams;
});*/

app.filter('breakline',function(){
	return function(text){
		if(text!=null)
			return text.replace(/\n/g,'<br>');
		else return "";
	}
});

app.filter('controlerDate', function(){
	return function (date, part){
		let options = {year:'numeric', month:'long', day:'numeric'};
		let d = date.toLocaleString('es-MX',options).split(/ de /g);
		switch(part){
			case 'day':
				return d[0];	
				break;
			case 'month':
				return d[1];	
				break;
			case 'year':
				return d[2];	
				break;
		}
		
	}
});